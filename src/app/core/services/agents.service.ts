import { environment } from './../../../environments/environment';
import { Agent } from './../../modules/auth/_models/agent.model';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

const URL_AGENT = environment.urlUser + '/agents';

@Injectable({
  providedIn: 'root'
})
export class AgentsService {
  agent$: BehaviorSubject<Agent>;

  constructor(private http: HttpClient) {
    this.agent$ = new BehaviorSubject<Agent>(undefined);
  }

  getTeamPerResponsable(matricule: number):Observable<Agent[]> {
    return this.http.get<any>(`${URL_AGENT}/equipe/responsable/${matricule}`);
  }

  getAgentByMatricule(matricule: number): Observable<Agent> {
    return this.http.get<Agent>(`${URL_AGENT}/${matricule}`);
  }

  getAllDirections() {
    return this.http.get(`${URL_AGENT}/directions`);
  }
}
