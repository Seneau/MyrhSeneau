export enum AppStateEnum {
    LOADED, LOADING, ERROR
}

export interface AppDataState<T> {
    state: AppStateEnum;
    data?: T;
    errorMessage?: string;
}