import { Pipe, PipeTransform } from '@angular/core';
import { CongeModel } from 'src/app/modules/conges/_models/conge.model';

@Pipe({
  name: 'datesearch'
})
export class DatesearchPipe implements PipeTransform {

  transform(conges:CongeModel[], dateInput: any): any[]{         
  
    
    if(!dateInput) {
        return  conges;
    }
       return conges.filter(
           x =>(new Date (x.dateDebut)).getMonth()==dateInput-1)
       
     }

}
