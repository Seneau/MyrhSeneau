import { Input, Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {
  
  transform(users:[], searchInput: string): any[]{ 
    
        
    if(!searchInput) {
        return  users;
    }
    searchInput = searchInput.toLowerCase();
       return users.filter(
           x =>(x['agent']['nom'] as string).toLowerCase().includes(searchInput)
       )
     }
}


