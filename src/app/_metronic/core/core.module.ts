import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FirstLetterPipe } from './pipes/first-letter.pipe';
import { SafePipe } from './pipes/safe.pipe';
import { SearchPipe } from './pipes/search.pipe';
import { DatesearchPipe } from './pipes/datesearch.pipe';
import { ListFilterPipe } from './pipes/list-filter.pipe';

@NgModule({

  declarations: [FirstLetterPipe, SafePipe ,SearchPipe,DatesearchPipe,ListFilterPipe],
  imports: [CommonModule],
  exports: [FirstLetterPipe, SafePipe ,SearchPipe,DatesearchPipe,ListFilterPipe],
})
export class CoreModule { }
