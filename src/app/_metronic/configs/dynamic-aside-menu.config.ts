export const DynamicAsideMenuConfig = {
  items: [
    {
      title: 'Dashboard',
      root: true,
      icon: 'flaticon2-architecture-and-city',
      svg: './assets/media/svg/icons/Design/Layers.svg',
      page: '/dashboard',
      translate: 'MENU.DASHBOARD',
      bullet: 'dot',
    },
    {
      title: 'Congés',
      root: true,
      bullet: 'dot',
      icon: 'flaticon2-list-2',
      svg: './assets/media/svg/icons/Code/Warning-2.svg',
      // page: '/conges',
      submenu: [
        {
          title: 'Demandes',
          page: '/conges/demandes'
        },
        {
          title: 'Rappels',
          page: '/conges/rappels'
        },
        {
          title: 'Validation',
          page: '/conges/validations'
        },
        {
          title: 'Gestion de Paie',
          page: '/conges/paye'
        }
      ]
    },
  ]
};
