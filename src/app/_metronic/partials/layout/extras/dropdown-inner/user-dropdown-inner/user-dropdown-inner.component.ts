import { AuthwithkcService } from './../../../../../../modules/auth/_services/authwithkc.service';
import { KeycloakService } from 'keycloak-angular';
import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { LayoutService } from '../../../../../core';
import { UserModel } from '../../../../../../modules/auth/_models/user.model';
import { KeycloackSecurityService } from 'src/app/modules/auth/_services/keycloak-security.service';
@Component({
  selector: 'app-user-dropdown-inner',
  templateUrl: './user-dropdown-inner.component.html',
  styleUrls: ['./user-dropdown-inner.component.scss'],
})
export class UserDropdownInnerComponent implements OnInit {
  extrasUserDropdownStyle: 'light' | 'dark' = 'light';
  user$: Observable<UserModel>;

  constructor(private layout: LayoutService, private auth: AuthwithkcService) { }

  ngOnInit(): void {
    this.extrasUserDropdownStyle = this.layout.getProp(
      'extras.user.dropdown.style'
    );
    this.user$ = this.auth.getCurrentUser();
  }

  logout() {
    this.auth.logout();
  }
  profil(){
    // this.ks.kc.accountManagement();
  }
}
