import { KeycloackSecurityService } from 'src/app/modules/auth/_services/keycloak-security.service';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { LayoutService } from '../../../../_metronic/core';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss'],
})
export class AsideComponent implements OnInit {
  roles: number[];
  
  disableAsideSelfDisplay: boolean;
  headerLogo: string;
  brandSkin: string;
  ulCSSClasses: string;
  location: Location;
  asideMenuHTMLAttributes: any = {};
  asideMenuCSSClasses: string;
  asideMenuDropdown;
  brandClasses: string;
  asideMenuScroll = 1;
  asideSelfMinimizeToggle = false;

  
  rolepaie = ['ROLE_AGENT_PAIE'];
  roledirecteur = ['ROLE_DIRECTEUR'];

  constructor(private layout: LayoutService, private loc: Location, private ks: KeycloackSecurityService) { }

  ngOnInit(): void {
    //this.roles = this.auth.currentUserValue.roles;
    
    // load view settings
    this.disableAsideSelfDisplay = this.layout.getProp('aside.self.display') === false;
    this.brandSkin = this.layout.getProp('brand.self.theme');
    this.headerLogo = this.getLogo();
    this.ulCSSClasses = this.layout.getProp('aside_menu_nav');
    this.asideMenuCSSClasses = this.layout.getStringCSSClasses('aside_menu');
    this.asideMenuHTMLAttributes = this.layout.getHTMLAttributes('aside_menu');
    this.asideMenuDropdown = this.layout.getProp('aside.menu.dropdown') ? '1' : '0';
    this.brandClasses = this.layout.getProp('brand');
    this.asideSelfMinimizeToggle = this.layout.getProp(
      'aside.self.minimize.toggle'
    );
    this.asideMenuScroll = this.layout.getProp('aside.menu.scroll') ? 1 : 0;


    // this.asideMenuCSSClasses = `${this.asideMenuCSSClasses} ${this.asideMenuScroll === 1 ? 'scroll my-4 ps ps--active-y' : ''}`;
    // Routing
    this.location = this.loc;
  }

  isVisible(roles: string[]): boolean{
    let returnBool = false;
    
    roles.forEach( roleReq => { 
        if (this.ks.kc.hasRealmRole(roleReq)){ 
            returnBool = true;
        }
    });
    return returnBool;
  }

  private getLogo() {
    return './assets/media/logos/seneau_logo_1.png';
  }
}
