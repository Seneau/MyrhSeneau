import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
colorPivot = '#FFFFFF';
  constructor() { }

  ngOnInit(): void {
  }

  changeColor(svg){
    svg.setAttribute('fill', '#FFFFFF');
  }
  retablirColor(svg, color){
    svg.setAttribute('fill', color);
  }
}
