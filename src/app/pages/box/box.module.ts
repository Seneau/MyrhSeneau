import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoxComponent } from './box.component';
import { RouterModule } from '@angular/router';
import { FileComponent } from '../file/file.component';
import { AttestRepriseComponent } from '../attest-reprise/attest-reprise.component';


@NgModule({
  declarations: [BoxComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: BoxComponent,
      },
      {
        path: 'file',
        component: FileComponent,
      },
      {
        path: 'attest-reprise',
        component: AttestRepriseComponent,
      },
    ])
  ]
})
export class BoxModule { }
