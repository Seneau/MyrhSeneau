import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttestRepriseComponent } from './attest-reprise.component';

describe('AttestRepriseComponent', () => {
  let component: AttestRepriseComponent;
  let fixture: ComponentFixture<AttestRepriseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AttestRepriseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttestRepriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
