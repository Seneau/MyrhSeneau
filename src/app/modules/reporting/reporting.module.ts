import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportingComponent } from './reporting.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgApexchartsModule } from 'ng-apexcharts';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { CoreModule } from 'src/app/_metronic/core';
import { ReportingRoutingModule } from './reporting-routing.module';
import { RcongesComponent } from './rconges/rconges.component';
import { RdeplacementComponent } from './rdeplacement/rdeplacement.component';
import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RlistCongesComponent } from './rconges/rlist-conges/rlist-conges.component';
import { RlistRecupComponent } from './rconges/rlist-recup/rlist-recup.component';
import { RlistRappelsComponent } from './rconges/rlist-rappels/rlist-rappels.component';

@NgModule({
  declarations: [ReportingComponent, RcongesComponent, RdeplacementComponent, RlistCongesComponent, RlistRecupComponent, RlistRappelsComponent],
  imports: [
    CommonModule,
    NgSelectModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    ReportingRoutingModule,
    CoreModule,
    FormsModule,
    NgApexchartsModule,
    ChartsModule ,
    NgbModule,

  ]
})
export class ReportingModule { }
