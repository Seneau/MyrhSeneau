import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RcongesComponent } from './rconges/rconges.component';
import { RdeplacementComponent } from './rdeplacement/rdeplacement.component';
import { ReportingComponent } from './reporting.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },   
  {
    path: 'dashboard',
    component: ReportingComponent ,
    children: [

      {
        path: '',
        redirectTo:'conge',
        pathMatch: 'full'
      },
      {
        path: 'conge',
        component: RcongesComponent
      },
      {
        path: 'deplacement',
        component: RdeplacementComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportingRoutingModule { }
