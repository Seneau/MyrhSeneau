import { UserModel } from './../auth/_models/user.model';
import { CongeModel } from './../conges/_models/conge.model';
import { Observable, of } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { CongesService } from './../conges/_services/conges.service';
import { concatMap, delay, startWith, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-reporting',
  templateUrl: './reporting.component.html',
  styleUrls: ['./reporting.component.scss']
})
export class ReportingComponent implements OnInit {
  

  constructor(private congesService: CongesService) { }
  currentTab = 'Reportingconge';
  change=false;
  ngOnInit(): void {
   this.change=false;
  }
  setCurrentTab(tab: string): void {
    this.currentTab = tab;
  }

  setCurrentTabC(tab: string): void {
    this.currentTab = tab;
    this.change=true; 
  }
}
