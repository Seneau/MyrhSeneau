import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RlistRecupComponent } from './rlist-recup.component';

describe('RlistRecupComponent', () => {
  let component: RlistRecupComponent;
  let fixture: ComponentFixture<RlistRecupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RlistRecupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RlistRecupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
