import { AuthwithkcService } from './../../auth/_services/authwithkc.service';
import { UserModel } from './../../auth/_models/user.model';
import { AppStateEnum, AppDataState } from './../../../core/state/app-data.state';
import { CongeModel } from './../../conges/_models/conge.model';
import { Observable, of } from 'rxjs';
import { Component, Input, OnInit } from '@angular/core';
import { CongesService } from './../../conges/_services/conges.service';
import { concatMap, delay, startWith, catchError, map } from 'rxjs/operators';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { MultiDataSet,Label, Colors, Color } from 'ng2-charts';
import { RappelService } from '../../conges/_services/rappel.service';
import { RappelModel } from '../../conges/_models/rappel.model';
import { RecuperationModel } from '../../conges/_models/recuperation.model';
import { CongeDataService } from '../../conges/_services/conge-data.service';
import { KeycloackSecurityService } from '../../auth/_services/keycloak-security.service';



@Component({
  selector: 'app-rconges',
  templateUrl: './rconges.component.html',
  styleUrls: ['./rconges.component.scss']
})
export class RcongesComponent implements OnInit {
  months = [
    {value: 1, libelle: 'Janvier'}, 
    {value: 2, libelle: 'Février'}, 
    {value: 3, libelle: 'Mars'}, 
    {value: 4, libelle: 'Avril'}, 
    {value: 5, libelle: 'Mai'}, 
    {value: 6, libelle: 'Juin'}, 
    {value: 7, libelle: 'Juillet'}, 
    {value: 8, libelle: 'Aout'}, 
    {value: 9, libelle: 'Septembre'}, 
    {value: 10, libelle: 'Octobre'}, 
    {value: 11, libelle: 'Novembre'}, 
    {value: 12, libelle: 'Décembre'}
  ];
  searchInput: string;
  dateInput: string;
  readonly StateEnum = AppStateEnum;
  conges$: Observable<AppDataState<CongeModel[]>>= new Observable<AppDataState<CongeModel[]>>();
  currentUser: UserModel;
  rappel:RappelModel[];
  recup:RecuperationModel[];
  nbRappelAn:Number;
  nbRappelMois:number;
  nbRecup:number;
  nbConge:number;
  conges:any;
  currentTab = 'conges';
  barConge= [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  chartDoughnut= [];

  //
  congesAn: CongeModel[];
  nbCongeAn: Number;
  nbCongeMois: Number;

  page = 1
  pageSize =6;
  item : CongeModel[];
  items : CongeModel[];
  collectionSize:number=0;
  

  constructor(private congesService: CongesService, private authServ: AuthwithkcService ,
    private rappelService:RappelService , private cdataService :CongeDataService,
    private ks: KeycloackSecurityService
    ) {
      
   }
  
 // currentTab = 'Reportingconge';
ngOnInit(): void {

  if (this.ks.kc) {
    if(this.ks.kc.hasRealmRole('ROLE_MANAGER')){
       // let typevar = this.ks.kc.hasRealmRole('ROLE_MANAGER')? 'responsable': 'direction' ;
       this.congesService.getCongesByResponsable(this.authServ.currentUserValue.attributes.matricule[0]);
       this.conges$ = this.congesService.conges$.pipe(
        
         concatMap(el => of({state: this.StateEnum.LOADED, data: el})),
         startWith(({state: this.StateEnum.LOADING})),
         catchError(err => of({state: this.StateEnum.ERROR, errorMessage: err.message}))
         );
   
       // le traitement pour la pagination
   
        
        this.congesService.getCountCongesMonthByManager(this.authServ.currentUserValue.attributes.matricule[0]).subscribe(
          number => this.nbCongeMois = number
        );

        // on va recuperer le nombre de rappel par mois de du manager 
        this.rappelService.getCountRappelMonthByManager(this.authServ.currentUserValue.attributes.matricule[0]).subscribe(
          number => this.nbRappelMois = number
        );

        // ON va recuperer ici le nombre de rappel de l année en cours du manager
        this.rappelService.getRappelsAnByManager(this.authServ.currentUserValue.attributes.matricule[0]).subscribe(
          rap => this.nbRappelAn = rap.length
        );
  }
  if(this.ks.kc.hasRealmRole('ROLE_MANAGER_PAIE') || this.ks.kc.hasRealmRole('ROLE_AGENT_PAIE') || this.ks.kc.hasRealmRole('ROLE_DRHT'))
  { 
          this.congesService.getAllCongesAn(3);
          this.conges$ = this.congesService.conges$.pipe(
          concatMap(el => of({state: this.StateEnum.LOADED, data: el})),
          startWith(({state: this.StateEnum.LOADING})),
          catchError(err => of({state: this.StateEnum.ERROR, errorMessage: err.message}))
          );       
            this.congesService.getAllCongesMois().subscribe(
              conges => {
                this.nbCongeMois = conges.length
              }
            )
        
            // --------------------------------------------
            /// recuperer le nombre d'agent rappelés par an
            //---------------------------------------------
            this.rappelService.getRappelsAn().subscribe(
              rappel => {
                this.rappel=rappel;
                this.nbRappelAn=this.rappel.length;

            })

            // -----------------------------------------------------
            /// recuperer le nombre d'agent rappelés le mois encours
            //------------------------------------------------------
            this.rappelService.getRappelsMois().subscribe(
              rappel => {
                this.rappel=rappel;
                this.nbRappelMois=this.rappel.length;
        
            })

          }
          if(this.ks.kc.hasRealmRole('ROLE_DIRECTEUR')){
            this.congesService.getCongesByDirection(this.authServ.currentUserValue.attributes.matricule[0]);
            this.conges$ = this.congesService.conges$.pipe(
              concatMap(el => of({state: this.StateEnum.LOADED, data: el})),
              startWith(({state: this.StateEnum.LOADING})),
              catchError(err => of({state: this.StateEnum.ERROR, errorMessage: err.message}))
              );
        

                // this.congesService.getCongesByDirecteur(this.authServ.currentUserValue.attributes.matricule[0]).subscribe(
                //   congesDir => this.nbCongeAn = congesDir.length
                // );
              
              // recupérons le nombre de rappel de l année  du directeur 
              this.rappelService.getRappelsAnByDirecteur(this.authServ.currentUserValue.attributes.matricule[0]).subscribe(
                rap => this.nbRappelAn = rap.length
              );
              
              // recupérons le nombre de rappel du mois du directeur 
              this.rappelService.getCountRappelsMonthByDirecteur(this.authServ.currentUserValue.attributes.matricule[0]).subscribe(
                rap => this.nbRappelMois = rap
              );

              // recupérons le nombre de congé du  mois du directeur
              this.congesService.getCountCongesMonthByDirecteur(this.authServ.currentUserValue.attributes.matricule[0]).subscribe(
                rap => this.nbCongeMois = rap
              ); 



          }
          
          this.congesService.getCongesByMonth().subscribe(
            (data)=>{
                data.forEach( month => {

                  this.barConge[month[0]-1] = month[1];
                });
                
                this.barChartData = [
                      { data: this.barConge, label: 'Mois' }
                    ];
                
                
            }
          );

          this.congesService.getCountCongesByDirection().subscribe(
            (data)=>{ 
              data.forEach( dir => {
                this.doughnutChartLabels.push(dir[0]);
                this.doughnutChartData.push([dir[1]]);
              });
                
            })
         
            // on va essayer à partir de la de recuperer toute les recupérations 
  }


     


      
    // là on va essayer lister les agents qui on une recupération
    // this.rappelService.getRecupérations().
    // pipe(
    //   map(rec => rec.filter(c => c.tracker.step === 2)) , 
    //   ).
    // subscribe(recup=>{
    //   this.recup=recup;
    //   this.nbRecup=this.recup.length;
    // })

}

search(value: string): void {
  this.item = this.items.filter((val) => val.agent.nom.toLowerCase().includes(value));
  this.collectionSize = this.item.length;
}

setCurrentTab(tab: string): void {
  this.currentTab = tab;
}

// géons les charts par là 
  barChartOptions: ChartOptions = {
    responsive: true,
  };
  barChartLabels: Label[] = ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil', 'Aout', 'Sep', 'Oct', 'Nov', 'Dec'];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];
  barChartColors: Color[]  =[{
    backgroundColor:"green",
    hoverBackgroundColor:"yellow",
    borderColor:"#0F0",
    hoverBorderColor:"#00F"
}];

// barChartData: ChartDataSets[] = [
//   { data: [20, 22, 30, 28, 25, 26, 30, 28, 22, 24, 25, 35], label: 'Mois' }
// ];
barChartData: ChartDataSets[] = [
  { data: this.barConge, label: 'Mois' }
];

  // la partie pour le deuxieme  chart 

  doughnutChartLabels: Label[] = [];
  doughnutChartData: MultiDataSet = [
  ];
  doughnutChartType: ChartType = 'doughnut';
  doughnutChartColors: Color[]  =[
    {
      backgroundColor:["#88CB3A",'#2C2875','#EFEFF9'],
      hoverBackgroundColor:"yellow",
      borderColor:"#2C2875",
      hoverBorderColor:"#00F"
    }
];

//--------------------------
//  GET number congé mois encours
//--------------------------

  getNumberCongeCurrentMonth(conges: CongeModel[])
  {
    const cuurentDate = new Date();
   let congeData= conges.filter(
      conge => (new Date (conge.dateDebut )).getMonth() == cuurentDate.getMonth() && conge.tracker.id ===4
    );

    return congeData.length;
    

  }

  isVisible(roles: string[]): boolean{
    let returnBool = false;
    if (this.ks.kc) {
      roles.forEach( roleReq => { 
          if (this.ks.kc.hasRealmRole(roleReq)){ 
              returnBool = true;
          }
      });
    }
    return returnBool;
  }

  fiterByMonth(conges: CongeModel[]){
    let data = [0,0,0,0,0,0,0,0,0,0,0,0];
    conges.forEach(conge=>{
        let numberMonth = (new Date(conge.dateDebut)).getMonth();
        data[numberMonth]+=1;
    })
    
 
    return data;
  }

 
}


