import { AuthwithkcService } from './../../../auth/_services/authwithkc.service';
import { UserModel } from './../../../auth/_models/user.model';
import { AppStateEnum, AppDataState } from './../../../../core/state/app-data.state';
import { CongeModel } from './../../../conges/_models/conge.model';
import { Observable, of } from 'rxjs';
import { Component, Input, OnInit } from '@angular/core';
import { CongesService } from './../../../conges/_services/conges.service';
import { concatMap, delay, startWith, catchError, map } from 'rxjs/operators';
import { RappelService } from '../../../conges/_services/rappel.service';
import { RappelModel } from '../../../conges/_models/rappel.model';
import { RecuperationModel } from '../../../conges/_models/recuperation.model';
import { CongeDataService } from '../../../conges/_services/conge-data.service';
import { KeycloackSecurityService } from '../../../auth/_services/keycloak-security.service';
@Component({
  selector: 'app-rlist-rappels',
  templateUrl: './rlist-rappels.component.html',
  styleUrls: ['./rlist-rappels.component.scss']
})
export class RlistRappelsComponent implements OnInit {
  months = [
    {value: 1, libelle: 'Janvier'}, 
    {value: 2, libelle: 'Février'}, 
    {value: 3, libelle: 'Mars'}, 
    {value: 4, libelle: 'Avril'}, 
    {value: 5, libelle: 'Mai'}, 
    {value: 6, libelle: 'Juin'}, 
    {value: 7, libelle: 'Juillet'}, 
    {value: 8, libelle: 'Aout'}, 
    {value: 9, libelle: 'Septembre'}, 
    {value: 10, libelle: 'Octobre'}, 
    {value: 11, libelle: 'Novembre'}, 
    {value: 12, libelle: 'Décembre'}
  ];
  searchInput: string;
  dateInput: number;
  readonly StateEnum = AppStateEnum;
  rappel$: Observable<AppDataState<RappelModel[]>>= new Observable<AppDataState<RappelModel[]>>();
  currentUser: UserModel;
  rappel:RappelModel[];
  
  
  page = 1
  pageSize =6;
  item : RappelModel[];
  items : RappelModel[];
  collectionSize:number=0;
  constructor(private congesService: CongesService, private authServ: AuthwithkcService ,
    private rappelService:RappelService , private cdataService :CongeDataService,
    private ks: KeycloackSecurityService) { }

  ngOnInit(): void {
    if(this.ks.kc.hasRealmRole('ROLE_MANAGER_PAIE') || this.ks.kc.hasRealmRole('ROLE_AGENT_PAIE') || this.ks.kc.hasRealmRole('ROLE_DRHT'))
    { 
      this.rappelService.getRappelsAnB();
      this.rappel$ = this.rappelService.rappels$.pipe(
      concatMap(el => of({state: this.StateEnum.LOADED, data: el})),
      startWith(({state: this.StateEnum.LOADING})),
      catchError(err => of({state: this.StateEnum.ERROR, errorMessage: err.message}))
      );
        //this.pageSize=10;
        // le traitement pour la pagination
        this.rappel$.subscribe(el => {
          if(el.data!== undefined){
            this.items=el.data
            this.item=this.items;      
          }
         });
  
    }
  
    if(this.ks.kc.hasRealmRole('ROLE_MANAGER')){
        
      // let typevar = this.ks.kc.hasRealmRole('ROLE_MANAGER')? 'responsable': 'direction' ;
      this.rappelService.getRappelsByManager(this.authServ.currentUserValue.attributes.matricule[0]);
       this.rappel$ = this.rappelService.rappels$.pipe(
        concatMap(el => of({state: this.StateEnum.LOADED, data: el})),
        startWith(({state: this.StateEnum.LOADING})),
        catchError(err => of({state: this.StateEnum.ERROR, errorMessage: err.message}))
        );
        
          // le traitement pour la pagination
          this.rappel$.subscribe(el => {
            if(el.data!== undefined){
              
              this.items=el.data
              this.item=this.items;      
            }
           });
   
   
     };
     if(this.ks.kc.hasRealmRole('ROLE_DIRECTEUR')){
        
      this.rappelService.getRappelsByDirecteur(this.authServ.currentUserValue.attributes.matricule[0])
       this.rappel$ = this.rappelService.rappels$.pipe(
        concatMap(el => of({state: this.StateEnum.LOADED, data: el})),
        startWith(({state: this.StateEnum.LOADING})),
        catchError(err => of({state: this.StateEnum.ERROR, errorMessage: err.message}))
        );
        
          // le traitement pour la pagination
          this.rappel$.subscribe(el => {
            
            if(el.data!== undefined){
               
              this.items=el.data
              this.item=this.items;      
            }
           });
   
   
     };
  }
  search(value: string): void {
    this.item = this.items.filter((val) => val.agent.nom.toLowerCase().includes(value));
    this.collectionSize = this.item.length;
  }
     searchDate(value:string) {
      if(value!=null) {
       this.item=this.items.filter(
         x =>(new Date (x.dateRetour)).getMonth()==this.dateInput-1)
         this.collectionSize = this.item.length;
     }
     else {
      this.item=this.items;
      this.collectionSize = this.item.length;
      
     }
     
   }  
  isVisible(roles: string[]): boolean{
    let returnBool = false;
    
    roles.forEach( roleReq => { 
        if (this.ks.kc.hasRealmRole(roleReq)){ 
            returnBool = true;
        }
    });
    return returnBool;
  }
}
