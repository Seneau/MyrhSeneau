import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RlistRappelsComponent } from './rlist-rappels.component';

describe('RlistRappelsComponent', () => {
  let component: RlistRappelsComponent;
  let fixture: ComponentFixture<RlistRappelsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RlistRappelsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RlistRappelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
