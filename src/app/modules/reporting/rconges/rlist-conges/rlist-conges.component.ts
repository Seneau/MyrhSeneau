import { AuthwithkcService } from './../../../auth/_services/authwithkc.service';
import { UserModel } from './../../../auth/_models/user.model';
import { AppStateEnum, AppDataState } from './../../../../core/state/app-data.state';
import { CongeModel } from './../../../conges/_models/conge.model';
import { Observable, of } from 'rxjs';
import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { CongesService } from './../../../conges/_services/conges.service';
import { concatMap, delay, startWith, catchError, map } from 'rxjs/operators';
import { RappelService } from '../../../conges/_services/rappel.service';
import { RappelModel } from '../../../conges/_models/rappel.model';
import { RecuperationModel } from '../../../conges/_models/recuperation.model';
import { CongeDataService } from '../../../conges/_services/conge-data.service';
import { KeycloackSecurityService } from '../../../auth/_services/keycloak-security.service';
import { ExcelService } from 'src/app/modules/conges/_services/excel.service';
@Component({
  selector: 'app-rlist-conges',
  templateUrl: './rlist-conges.component.html',
  styleUrls: ['./rlist-conges.component.scss']
})
export class RlistCongesComponent implements OnInit {
  months = [
    {value: 1, libelle: 'Janvier'}, 
    {value: 2, libelle: 'Février'}, 
    {value: 3, libelle: 'Mars'}, 
    {value: 4, libelle: 'Avril'}, 
    {value: 5, libelle: 'Mai'}, 
    {value: 6, libelle: 'Juin'}, 
    {value: 7, libelle: 'Juillet'}, 
    {value: 8, libelle: 'Aout'}, 
    {value: 9, libelle: 'Septembre'}, 
    {value: 10, libelle: 'Octobre'}, 
    {value: 11, libelle: 'Novembre'}, 
    {value: 12, libelle: 'Décembre'}
  ];
  searchInput: string;
  dateInput: number;
  readonly StateEnum = AppStateEnum;
  conges$: Observable<AppDataState<CongeModel[]>>= new Observable<AppDataState<CongeModel[]>>();
  currentUser: UserModel;
  rappel:RappelModel[];
  recup:RecuperationModel[];
  tabExcel =[];
  page = 1
  pageSize =6;
  item : CongeModel[];
  items : CongeModel[];
  collectionSize:number=0;
  constructor(private congesService: CongesService, private authServ: AuthwithkcService ,
    private rappelService:RappelService , private cdataService :CongeDataService,
    private ks: KeycloackSecurityService ,private excelService:ExcelService
    ) {
     }

    ngOnInit(): void {

      if(this.ks.kc.hasRealmRole('ROLE_MANAGER')){
        
       // let typevar = this.ks.kc.hasRealmRole('ROLE_MANAGER')? 'responsable': 'direction' ;
       this.congesService.getCongesByResponsable(this.authServ.currentUserValue.attributes.matricule[0]);
       this.conges$ = this.congesService.conges$.pipe(
        
         concatMap(el => of({state: this.StateEnum.LOADED, data: el})),
         startWith(({state: this.StateEnum.LOADING})),
         catchError(err => of({state: this.StateEnum.ERROR, errorMessage: err.message}))
         );
    
        // le traitement pour la pagination
        this.conges$.subscribe(el => {
          if(el.data!== undefined){
            this.items=el.data
            this.item=this.items;      
          }
         });
    
    
      };
      if(this.ks.kc.hasRealmRole('ROLE_DIRECTEUR')){
        this.congesService.getCongesByDirection(this.authServ.currentUserValue.attributes.matricule[0]);
        this.conges$ = this.congesService.conges$.pipe(
          concatMap(el => of({state: this.StateEnum.LOADED, data: el})),
          startWith(({state: this.StateEnum.LOADING})),
          catchError(err => of({state: this.StateEnum.ERROR, errorMessage: err.message}))
          );
           // le traitement pour la pagination
        this.conges$.subscribe(el => {
          if(el.data!== undefined){
            this.items=el.data
            this.item=this.items;      
          }
         });
          
      }
      if(this.ks.kc.hasRealmRole('ROLE_MANAGER_PAIE') || this.ks.kc.hasRealmRole('ROLE_AGENT_PAIE') || this.ks.kc.hasRealmRole('ROLE_DRHT'))
      { 
        this.congesService.getAllCongesAn(3);
        this.conges$ = this.congesService.conges$.pipe(
        concatMap(el => of({state: this.StateEnum.LOADED, data: el})),
        startWith(({state: this.StateEnum.LOADING})),
        catchError(err => of({state: this.StateEnum.ERROR, errorMessage: err.message}))
        );
          
          // le traitement pour la pagination
          
          this.conges$.subscribe(el => {
            if(el.data!== undefined){
              this.items=el.data
              this.item=this.items;      
            }
           });
    
      }
    
  
          
        
      
    
    
    
    
    }
    
  
    search(value: string): void {
      this.item = this.items.filter((val) => val.agent.nom.toLowerCase().includes(value));
    this.collectionSize = this.item.length;
    }
    searchDate(value:string) {
      if(value!=null) {
       this.item=this.items.filter(
         x =>(new Date (x.dateDebut)).getMonth()==this.dateInput-1)
         this.collectionSize = this.item.length;
     }
     else {
      this.item=this.items;
      this.collectionSize = this.item.length;
      
     }
     
   }  
  

    isVisible(roles: string[]): boolean{
      let returnBool = false;
      
      roles.forEach( roleReq => { 
          if (this.ks.kc.hasRealmRole(roleReq)){ 
              returnBool = true;
          }
      });
      return returnBool;
    }
    exportAsXLSX():void {

      for (let element in this.item) {  
           
        this.tabExcel.push({  
            'matricule': this.item[element].matricule,  
            'nom':this.item[element].agent.nom,
            'DateDebut':this.item[element].dateDebut,
            'DateRetourEffective':this.item[element].dateRetourEffective,
            'NbJrCongé':this.item[element].nombreJourConge
           
        });  
    }
  
      
    this.excelService.exportAsExcelFile(this.tabExcel, 'Liste congés');
   }
}
