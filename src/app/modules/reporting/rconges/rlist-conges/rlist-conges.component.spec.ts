import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RlistCongesComponent } from './rlist-conges.component';

describe('RlistCongesComponent', () => {
  let component: RlistCongesComponent;
  let fixture: ComponentFixture<RlistCongesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RlistCongesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RlistCongesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
