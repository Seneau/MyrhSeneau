import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RdeplacementComponent } from './rdeplacement.component';

describe('RdeplacementComponent', () => {
  let component: RdeplacementComponent;
  let fixture: ComponentFixture<RdeplacementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RdeplacementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RdeplacementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
