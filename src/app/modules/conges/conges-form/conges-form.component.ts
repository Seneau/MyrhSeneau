import { TrackersService } from './../_services/trackers.service';
import { AuthwithkcService } from './../../auth/_services/authwithkc.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CongesService } from './../_services/conges.service';
import { CongeModel } from './../_models/conge.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Agent } from './../../auth/_models/agent.model';
import { Observable } from 'rxjs';
import { AgentsService } from './../../../core/services/agents.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-conges-form',
  templateUrl: './conges-form.component.html',
  styleUrls: ['./conges-form.component.scss']
})
export class CongesFormComponent implements OnInit {
  titre = 'Nouvelle demande de congé';
  isEdit = false;
  congeUpdate: CongeModel;
  formConge: FormGroup;
  agents$: Observable<Agent[]>;
  matricule: number;

  constructor(
    private fb: FormBuilder,
    private authService: AuthwithkcService,
    private congesService: CongesService,
    private router: Router,
    private route: ActivatedRoute,
    private agentService: AgentsService,
    private trackersServ: TrackersService
  ) { 
    this.formConge = this.fb.group({
      matricule: [null, Validators.required],
      dateDebut: [null, Validators.required]
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      if (params.id) {
        this.titre = 'Reprogrammer la demande de congé';
        this.isEdit = true;
        this.congesService.getCongeById(params.id).subscribe(conge => {
          this.congeUpdate = conge;
          this.formConge.get('matricule').setValue(this.congeUpdate.matricule);
          this.formConge.get('dateDebut').setValue(this.congeUpdate.dateDebut);
        });
      }
    });

    if (this.authService.currentUserValue) {
      this.matricule = this.authService.currentUserValue.attributes.matricule;
      
      this.agents$ = this.agentService.getTeamPerResponsable(this.matricule);
    }
  }

  get f(): any { return this.formConge.controls; }

  onSubmitConge(): void {
    
    if(this.formConge.invalid) return;
    if (!this.datesFilter(this.formConge.value.dateDebut)) return;

    if(this.isEdit) {
      if (this.formConge.invalid || this.formConge.value.matricule !== this.congeUpdate.matricule) return;

      this.congeUpdate.dateDebut = this.formConge.value.dateDebut;
      this.trackersServ.getTrackerStep(1).subscribe(tracker => {
        this.congeUpdate.tracker = tracker;
        this.congesService.createConge(this.congeUpdate).subscribe(el => {
          if (el) {
            this.congesService.resetNombreJourConge(this.congeUpdate.matricule);
            this.congesService.getCongesByResponsable(this.matricule);
            this.router.navigateByUrl('/conges');
          }
        })
      });
    }else {

      const conge: CongeModel = this.formConge.value;
      this.trackersServ.getTrackerStep(1).subscribe(tracker => {
        conge.tracker = tracker;
        conge.nombreJourConge = 0;
        this.congesService.createConge(conge).subscribe(el => {
          if (el) {
            // this.congesService.getCongesByResponsable(this.matricule);
            this.congesService.resetNombreJourConge(el.matricule);
            this.congesService.getCongesByResponsable(this.matricule);
            this.router.navigateByUrl('/conges');
          }
        })
      });
  
    }

  }


  datesFilter = (d: Date): boolean => {
    if (d) {
      
      const day = d.getDay();
      const now = new Date();
      
      const weekend = day !== 0 && day !== 6;

      if (d.getMonth() === 12) {
        return weekend && d.getFullYear() < now.getFullYear() + 1 && d > now;
      }

      return weekend && d.getFullYear() <= now.getFullYear() && d > now;
    }
    
    // return d > (new Date()) && weekend ;
  }

}
