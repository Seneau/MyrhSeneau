import { environment } from './../../../../environments/environment';
import { map } from 'rxjs/operators';
import { AgentsService } from './../../../core/services/agents.service';
import { RecuperationModel } from './../_models/recuperation.model';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";

const ROUTE_RECUP = environment.urlConge + '/recuperations';

@Injectable({providedIn: 'root'})
export class RecuperationsService {
    recupSubject = new BehaviorSubject<RecuperationModel[]>([]);
    recup$ = this.recupSubject.asObservable();

    constructor(private http: HttpClient, private agentService: AgentsService) {}

    getAllRecuperation(): Observable<RecuperationModel[]> {
        return this.http.get<RecuperationModel[]>(ROUTE_RECUP);
    }

    getRecuperationsByMatricule(matricule: number) {
        this.http.get<RecuperationModel[]>(`${ROUTE_RECUP}/matricule/${matricule}`)
            .pipe(
                map(el => {
                    return el.map(res => {
                        this.agentService.getAgentByMatricule(res.matricule).subscribe(agent => res.agent = agent);
                        return res;
                    })
                })
            )
            .subscribe(recups => this.recupSubject.next(recups));
    }

    getRecupsByParam(param: {type: 'direction' | 'responsable' | 'matricule', matricule: number}, step: number) {
        this.http.get<RecuperationModel[]>(`${ROUTE_RECUP}/${param.type}/${param.matricule}?step=${step}`)
            .subscribe(recups => this.recupSubject.next(recups));
    }
    
    getRecuperation(id: number): Observable<RecuperationModel> {
        return this.http.get<RecuperationModel>(`${ROUTE_RECUP}/${id}`);
    }
    
    updateRecuperation(data: any): Observable<any> {
        return this.http.put<any>(ROUTE_RECUP, data);
    }
    
    createRecuperation(data: any): Observable<any> {
        return this.http.post<any>(ROUTE_RECUP, data);
    }
    

    getAllRecuperationsYear(){
        this.http.get<RecuperationModel[]>(`${ROUTE_RECUP}`)
            .subscribe(el => this.recupSubject.next(el));
    }

    // getAllRecuperationsYearByManager(matricule: number){
    //     this.http.get<RecuperationModel[]>(`${ROUTE_RECUP}/responsable/${matricule}`)
    //         .subscribe(el => this.recupSubject.next(el));
    // }

    // getAllRecuperationsYearByDirecteur(matricule: number){
    //     this.http.get<RecuperationModel[]>(`${ROUTE_RECUP}/direction/${matricule}`)
    //         .subscribe(el => this.recupSubject.next(el));
    // }
}
