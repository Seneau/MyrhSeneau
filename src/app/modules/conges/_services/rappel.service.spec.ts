import { environment } from './../../../../environments/environment';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, getTestBed } from '@angular/core/testing';

import { RappelService } from './rappel.service';

describe('RappelService', () => {
  let service: RappelService;
  let httpMock: HttpTestingController;
  let injector: TestBed;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    injector = getTestBed();
    service = injector.inject(RappelService);
    httpMock = injector.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getRappel', () => {

    it('Should return Observable<Rappel>', done => {
      const mockRappel = {
        "id": 1,
        "matricule": 13170,
        "rappeleApres": 15,
        "dateRetour": "2021-07-19",
        "rappelePour": 5,
        "motif": "Motif rappel",
        "tracker": {
          "id": 3,
          "step": 2,
          "libelle": "validée"
        }
      };

      service.getRappel(1).subscribe(rappel => {
        expect(rappel).toBeDefined();
        done();
      });

      const req = httpMock.expectOne(`${environment.urlConge}/rappels/1`);
      expect(req.request.method).toBe('GET');
      req.flush(mockRappel);
    })
    
  });
});
