import { from, of } from 'rxjs';
import { environment } from './../../../../environments/environment';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { inject, TestBed, getTestBed } from '@angular/core/testing';

import { CongesService } from './conges.service';

describe('CongesService', () => {
  let service: CongesService;
  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CongesService]
    });
    injector = getTestBed();
    service = injector.get(CongesService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  })

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getConges', () => {

    it('Should return Observable<Conges[]>', (done) => {
      const mockConges = [{
        "id": 2,
        "matricule": 14117,
        "dateDebut": "2021-08-03",
        "dateRetourEffective": null,
        "tracker": {
          "id": 3,
          "step": 2,
          "libelle": "validée"
        },
        "rappel": null,
        "nombreJourConge": 0
      }, {
        "id": 2,
        "matricule": 14117,
        "dateDebut": "2021-08-03",
        "dateRetourEffective": null,
        "tracker": {
          "id": 3,
          "step": 2,
          "libelle": "validée"
        },
        "rappel": null,
        "nombreJourConge": 0
      }];

      service.getConges().subscribe({
        next: conges => {
          expect(conges).toBeDefined();
          expect(conges[0].id).toBe(2);
          done();
        } 
      })

      const req = httpMock.expectOne(`${environment.urlConge}/conges`);
      // expect(req.request.method).toBe('GET');
      req.flush(mockConges);

    })
  });

  describe('#getOneConge', () => {

    it('Should return Observable<Conge>', (done) => {
      const mockConge = {
        "id": 2,
        "matricule": 14117,
        "dateDebut": "2021-08-03",
        "dateRetourEffective": null,
        "tracker": {
          "id": 3,
          "step": 2,
          "libelle": "validée"
        },
        "rappel": null,
        "nombreJourConge": 0
      };
      
      service.getConge(2).subscribe({
        next: conge => {
          expect(conge).toBeDefined();
          expect(conge.id).toBe(2);
          done();
        }
      })

      const req = httpMock.expectOne(`${environment.urlConge}/conges/2`);
      req.flush(mockConge);
    })

  });

});
