import { Observable } from 'rxjs';
import { Tracker } from './../_models/tracker.model';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../../environments/environment';
import { Injectable } from '@angular/core';

const ROUTE_TRACKER = environment.urlConge + '/trackers';

@Injectable({providedIn: 'root'})
export class TrackersService {
    
    constructor(private http: HttpClient) {}

    getTrackerStep(step: number): Observable<Tracker> {
        return this.http.get<Tracker>(`${ROUTE_TRACKER}/${step}`);
    }
}
