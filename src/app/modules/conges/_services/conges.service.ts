import { AgentsService } from './../../../core/services/agents.service';
import { map } from 'rxjs/operators';
import { CongeModel } from './../_models/conge.model';
import { environment } from './../../../../environments/environment';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

const ROUTE_CONGE = `${environment.urlConge}/conges`;

@Injectable({providedIn: 'root'})
export class CongesService {
    congesSubject$ = new BehaviorSubject<CongeModel[]>([]);
    conges$: Observable<CongeModel[]> = this.congesSubject$.asObservable();
    
    constructor(private http: HttpClient, private agentService?: AgentsService) {}

    getAllConges(month?: number) {
        this.http.get<CongeModel[]>(ROUTE_CONGE)
        .pipe(
            map(el => {
                if (month) {
                    el = el.filter(x => {
                        return new Date(x.dateDebut).getMonth()+1 === month;
                    })   
                }
                return el.map(res => {
        
                    this.agentService.getAgentByMatricule(res.matricule).subscribe(agent => res.agent = agent);
                    return res;
                })
            })
        )
        .subscribe(el => this.congesSubject$.next(el));
    }
    
    getConges(){
       return this.http.get<CongeModel[]>(ROUTE_CONGE)    
    }

    getConge(id: number): Observable<CongeModel> {
        return this.http.get<CongeModel>(`${ROUTE_CONGE}/${id}`);
    }

    createConge(data: CongeModel): Observable<any> {
        return this.http.post<any>(ROUTE_CONGE, data);
    }

    updateConge(conge: CongeModel):Observable<any> {
        return this.http.put<any>(ROUTE_CONGE, conge);
    }

    getCongeById(id: number): Observable<CongeModel> {
        return  this.http.get<CongeModel>(`${ROUTE_CONGE}/${id}`)
        .pipe(
            map(el => {
                this.agentService.getAgentByMatricule(el.matricule).subscribe(res => el.agent = res);
                return el;
            })
        )
    }


    getCongesByMatricule(matricule: number) {
        this.http.get<CongeModel[]>(`${ROUTE_CONGE}/matricule/${matricule}`)
            .pipe(
                map(el => {
                    return el.map(res => {
                        this.agentService.getAgentByMatricule(res.matricule).subscribe(agent => res.agent = agent);
                        // this.congeData.getCongeDataByMatricule(res.matricule).subscribe(agent => res. = agent);
                        return res;
                    });
                })
            )
            .subscribe(el => {
                this.congesSubject$.next(el);
            });
    }
/*
    getCongesByResponsable(matricule: number, month?: number) {
        
        this.http.get<CongeModel[]>(`${ROUTE_CONGE}/responsable/${matricule}`)
            .pipe(
                map(el => {
                    if (month) {
                        el = el.filter(x => {
                            return new Date(x.dateDebut).getMonth()+1 === month;
                        })   
                    }
                    el.map(res => {            
                        this.agentService.getAgentByMatricule(res.matricule).subscribe(agent => res.agent = agent);
                        return res;
                    });
                    return el;
                })
                )
                .subscribe(el => this.congesSubject$.next(el));
        }

    getCongesByDirection(matricule: number) {
        this.http.get<CongeModel[]>(`${ROUTE_CONGE}/direction/${matricule}`)
            .pipe(
                map(el => {
                    return el.filter(c => c.tracker.step === 1)
                        .map(res => {
                        this.agentService.getAgentByMatricule(res.matricule).subscribe(agent => res.agent = agent);
                        return res;
                    })
                })
            )
            .subscribe(el => this.congesSubject$.next(el));
    }*/

    resetNombreJourConge(matricule: number) {
        this.http.get<CongeModel[]>(`${ROUTE_CONGE}/matricule/${matricule}`).subscribe(conges => {
            const tabs = conges
                .filter(c => c.tracker.step === 1 || c.tracker.step === 2)
                .sort((a,b) => new Date(a.dateDebut).getTime() - new Date(b.dateDebut).getTime());
            tabs.shift()
            tabs.map(conge => {
                conge.nombreJourConge = 0;
                this.updateConge(conge).subscribe(
                    () => console.log('updated'),
                    err => console.error(err)
                );
            });
        })
    }



    // AllS conges par an
    getAllCongesAn(step?){
        let path = `${ROUTE_CONGE}/an?step=${step}`
        this.http.get<CongeModel[]>(path)        
        .pipe(
            map(el => {
              
                return el;
            })
        )
        .subscribe(el => this.congesSubject$.next(el)); 
    }

    getAllCongesMois(): Observable<CongeModel[]>{
        let path = `${ROUTE_CONGE}/mois`;
       return this.http.get<CongeModel[]>(path);

    }

    getCongesByMonth(): Observable<any>{
        let path = `${ROUTE_CONGE}/count/mois`
        return this.http.get<any>(path);
    }

    getCountCongesByDirection():Observable<any>{
        let path = `${ROUTE_CONGE}/direction/count`
        return this.http.get<any>(path);
    }

    getCountCongesMonthByManager(matricule: number){
        return this.http.get<Number>(`${ROUTE_CONGE}/mois/responsable/${matricule}`);
    }

    getCountCongesMonthByDirecteur(matricule: number){
        return this.http.get<Number>(`${ROUTE_CONGE}/direction/count/mois/${matricule}`);
    }
    
    getCongesByDirection(matricule: number){
        this.http.get<CongeModel[]>(`${ROUTE_CONGE}/direction1/${matricule}`)
            .subscribe(el => this.congesSubject$.next(el));
    }

    getCongesByResponsable(matricule: number, month?: number) {
        this.http.get<CongeModel[]>(`${ROUTE_CONGE}/responsable1/${matricule}`)
            // .pipe(
            //     map(el => {
            //         if (month) {
            //             el = el.filter(x => {
            //                 return new Date(x.dateDebut).getMonth()+1 === month;
            //             })   
            //         }
            //         return el;
            //     })
            // )
            .subscribe(el => this.congesSubject$.next(el));
    }
}
