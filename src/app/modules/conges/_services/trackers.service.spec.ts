import { TrackersService } from './trackers.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

describe('TrackersService', () => {
  let service: TrackersService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(TrackersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
