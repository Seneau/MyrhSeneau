import { AgentsService } from './../../../core/services/agents.service';
import { map } from 'rxjs/operators';
import { environment } from './../../../../environments/environment';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RappelModel } from '../_models/rappel.model';
import { RecuperationModel } from '../_models/recuperation.model';

const ROUTE_RAPPEL = `${environment.urlConge}/rappels`;
const ROUTE_CDATA = `${environment.urlConge}/conge-data`;
const ROUTE_RECUP = `${environment.urlConge}/recuperations`;
@Injectable({
  providedIn: 'root'
})
export class RappelService {
  RappelSubject = new BehaviorSubject<RappelModel[]>([]);
  rappels$ = this.RappelSubject.asObservable();

  constructor(private http: HttpClient, private agentService: AgentsService) { }

  getAllRappelsBis() {
    this.http.get<RappelModel[]>(ROUTE_RAPPEL).pipe(
      map(el => {
        return el.map(res => {
          this.agentService.getAgentByMatricule(res.matricule).subscribe(agent => res.agent = agent);
          return res;
        })
      })
    ).subscribe(rappels => this.RappelSubject.next(rappels));
  }

  getRappelsByParam(param: {type: 'direction' | 'responsable' | 'matricule', matricule: number}) {
    this.http.get<RappelModel[]>(`${ROUTE_RAPPEL}/${param.type}/${param.matricule}`)
        .pipe(
            map(el => {
                return el.map(res => {
                    this.agentService.getAgentByMatricule(res.matricule).subscribe(agent => res.agent = agent);
                    return res;
                })
            })
        )
        .subscribe(el => {
            this.RappelSubject.next(el)
        });
  }

  updateRappel(rappel: RappelModel): Observable<RappelModel> {
    return this.http.put<RappelModel>(ROUTE_RAPPEL, rappel);
  }

  Rappeler(data: any): Observable<any> {
    return this.http.post<any>(ROUTE_RAPPEL, data);
  }

  getRappel(id: number): Observable<RappelModel> {
    return this.http.get<RappelModel>(`${ROUTE_RAPPEL}/${id}`);
  }

  getAllRappels(){
    return this.http.get<RappelModel[]>(ROUTE_RAPPEL)
      .pipe(
      map(el => {
          return el.map(res => {
              this.agentService.getAgentByMatricule(res.matricule).subscribe(agent => res.agent = agent);
              return res;
          })
      })
    );

  }

//------------------------------
// GET nombre de rappels par an
//------------------------------
getRappelsAn(){
  let path = `${ROUTE_RAPPEL}/an`
  return this.http.get<RappelModel[]>(path);
}

getRappelsAnB(){
  let path = `${ROUTE_RAPPEL}/an`
   this.http.get<RappelModel[]>(path)
   .pipe(
    map(el => {        
        return el;
    })
)
.subscribe(el => {
    this.RappelSubject.next(el)
});
}

//------------------------------
// GET nombre de rappels par mois
//------------------------------
getRappelsMois(){
  let path = `${ROUTE_RAPPEL}/mois`
  return this.http.get<RappelModel[]>(path)
  
}




// getAllRappels(){
//   this.http.get<RappelModel[]>(ROUTE_RAPPEL).subscribe(res => {
//     res.forEach(element => {
//        element.
//     });
//   })

// }

  getRecupérations(){
    return  this.http.get<RecuperationModel[]>(ROUTE_RECUP) 
  }

  getCountRappelMonthByManager(matricule: number){
    return this.http.get<number>(`${ROUTE_RAPPEL}/manager/count/mois/${matricule}`);
  }

  getRappelsAnByManager(matricule: number) {
    return this.http.get<RappelModel[]>(`${ROUTE_RAPPEL}/manager/year/${matricule}`);
  }

  getRappelsByManager(matricule: number) {
    this.http.get<RappelModel[]>(`${ROUTE_RAPPEL}/manager/year/${matricule}`)
    .pipe(
      map(el => {
          return el;
      })
  )
  .subscribe(el => {
      this.RappelSubject.next(el)
  });

  }
  getRappelsAnByDirecteur(matricule: number) {
    return this.http.get<RappelModel[]>(`${ROUTE_RAPPEL}/direction/year/${matricule}`);
  }
  getRappelsByDirecteur(matricule: number) {
    this.http.get<RappelModel[]>(`${ROUTE_RAPPEL}/direction/year/${matricule}`)
      .subscribe(el => this.RappelSubject.next(el));
  }

  getCountRappelsMonthByDirecteur(matricule: number) {
    return this.http.get<number>(`${ROUTE_RAPPEL}/directeur/count/mois/${matricule}`);
  }
}
