import { environment } from './../../../../environments/environment';
import { CongeDataModel } from './../_models/conge-data.model';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { map } from 'rxjs/operators';
import { AgentsService } from 'src/app/core/services/agents.service';

const ROUTE_CONGE_DATA = environment.urlConge + '/conge-data';

@Injectable({providedIn: 'root'})
export class CongeDataService {
    congeDataSubject = new BehaviorSubject<CongeDataModel>(undefined);
    congeData$ = this.congeDataSubject.asObservable();

    // test
    congesDataSubject$ = new BehaviorSubject<CongeDataModel[]>([]);
    congesData$: Observable<CongeDataModel[]> = this.congesDataSubject$.asObservable();

    constructor(private http: HttpClient,private agentService: AgentsService) {}
    getAllCongesData() {
        return this.http.get<CongeDataModel[]>(`${ROUTE_CONGE_DATA}`)
        .pipe(
            map(el => {
                      return el.map(res => {
                        this.agentService.getAgentByMatricule(res.matricule).subscribe(agent => res.agent = agent);
                        
                        return res;
                    })
              
            })
        )
        .subscribe(el => this.congesDataSubject$.next(el));
     
    }

    getCongeDataByMatricule(matricule: number) {
        this.http.get<CongeDataModel>(`${ROUTE_CONGE_DATA}/matricule/${matricule}`)
            .subscribe(cd => this.congeDataSubject.next(cd));
    }

    updateCongeData(congeData: CongeDataModel) {
        this.http.put<CongeDataModel>(`${ROUTE_CONGE_DATA}`, congeData)
            .subscribe(cd => this.congeDataSubject.next(cd));
    }

    getCongeData(matricule: number): Observable<CongeDataModel> {
        return this.http.get<CongeDataModel>(`${ROUTE_CONGE_DATA}/matricule/${matricule}`);
    }

    updateCD(congeData: CongeDataModel): Observable<CongeDataModel> {
        return this.http.put<CongeDataModel>(`${ROUTE_CONGE_DATA}`, congeData)
    }
}
