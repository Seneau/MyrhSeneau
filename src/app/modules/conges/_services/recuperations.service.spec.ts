import { environment } from './../../../../environments/environment';
import { RecuperationModel } from './../_models/recuperation.model';
import { RecuperationsService } from './recuperations.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, getTestBed } from '@angular/core/testing';

describe('RecuperationsService', () => {
  let service: RecuperationsService;
  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    injector = getTestBed();
    service = injector.inject(RecuperationsService);
    httpMock = injector.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getAllRecup', () => {

    it('Should return Observable<Recuperation[]>', done => {
      const mockRecups: RecuperationModel[] = [];

      service.getAllRecuperation();
      // expect(service.getAllRecuperation).toHaveReturned();

      const req = httpMock.expectOne(environment.urlConge + '/recuperations');
      req.flush(mockRecups);
    })
    
  });
});
