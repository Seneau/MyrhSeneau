import { AgentsService } from './../../../core/services/agents.service';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { Tracker } from './../_models/tracker.model';
import { CongeDataModel } from './../_models/conge-data.model';
import { CongeDataService } from './../_services/conge-data.service';
import { TrackersService } from './../_services/trackers.service';
import { AuthwithkcService } from "./../../auth/_services/authwithkc.service";
import { Component, OnInit } from "@angular/core";
import {
  AppStateEnum,
  AppDataState,
} from "./../../../core/state/app-data.state";
import { CongeModel } from "./../_models/conge.model";
import { Observable, of } from "rxjs";
import { CongesService } from "../_services/conges.service";
import { concatMap, delay, startWith, catchError, map } from "rxjs/operators";
import { NgbModal, NgbModalConfig } from "@ng-bootstrap/ng-bootstrap";
import { UserModel } from "../../auth";
import { ExcelService } from '../_services/excel.service';
@Component({
  selector: "app-conges-paie",
  templateUrl: "./conges-paie.component.html",
  styleUrls: ["./conges-paie.component.scss"],
})
export class CongesPaieComponent implements OnInit {
  months = [
    {value: 1, libelle: 'Janvier'}, 
    {value: 2, libelle: 'Février'}, 
    {value: 3, libelle: 'Mars'}, 
    {value: 4, libelle: 'Avril'}, 
    {value: 5, libelle: 'Mai'}, 
    {value: 6, libelle: 'Juin'}, 
    {value: 7, libelle: 'Juillet'}, 
    {value: 8, libelle: 'Aout'}, 
    {value: 9, libelle: 'Septembre'}, 
    {value: 10, libelle: 'Octobre'}, 
    {value: 11, libelle: 'Novembre'}, 
    {value: 12, libelle: 'Décembre'}
  ];
  searchInput: string;
  dateInput: number;
  checkAll = false;
  formCongesCloture: FormGroup;
  
  directions: any;
  currentUser: UserModel;
  congeData: CongeDataModel;
  readonly StateEnum = AppStateEnum;
  // conges$: Observable<AppDataState<CongeModel[]>>;
  rappels: Observable<AppDataState<CongeModel[]>>;
  page = 1
  pageSize =6;
  item : CongeModel[];
  items : CongeModel[];
  collectionSize:number=0;
  tabExcel =[];
  constructor(
    private fb: FormBuilder,
    private congesService: CongesService,
    private authServ: AuthwithkcService,
    private agentService: AgentsService,
    private trackerService: TrackersService,
    private congeDataServ: CongeDataService,
    config: NgbModalConfig,
    private modalService: NgbModal ,
    private excelService:ExcelService
  ) {
    config.backdrop = "static";
    config.keyboard = false;
    this.formCongesCloture = this.fb.group({
      checkCongesArray: this.fb.array([], Validators.required)
    })
  }

  openConfirmation(content) {
    this.modalService.open(content);
  }
  ngOnInit(): void {
    //this.dateInput = new Date().getMonth() + 1;
    this.currentUser = this.authServ.currentUserValue;
    this.congesService.getAllConges(this.dateInput);
    this.rappels = this.congesService.conges$.pipe(
      map((conges) => conges.filter((c) => [2,3].includes(c.tracker.step))),
      concatMap((el) =>
        of({ state: this.StateEnum.LOADED, data: el })
      ),
      startWith({ state: this.StateEnum.LOADING }),
      catchError((err) =>
        of({ state: this.StateEnum.ERROR, errorMessage: err.message })
      )
    );
    this.getDirections();
    this.rappels.subscribe(el => {
      if(el.data!== undefined){
        this.items=el.data
        this.item=this.items;      
      }
     });

  }

  searchDate(value:string) {
    if(value!=null) {
     this.item=this.items.filter(
       x =>(new Date (x.dateDebut)).getMonth()==this.dateInput-1)
       this.collectionSize = this.item.length;
   }
   else {
    this.item=this.items;
    this.collectionSize = this.item.length;
    
   }
   
 }  
  exportAsXLSX():void {

    for (let element in this.item) {  
      
      if(this.item[element].tracker.step==3) {
          this.tabExcel.push({  
            'matricule': this.item[element].matricule,  
            'nom':this.item[element].agent.nom,
            'DateDebut':this.item[element].dateDebut,
            'DateRetourEffective':this.item[element].dateRetourEffective,
            'NbJrCongé':this.item[element].nombreJourConge
          
        }); 
      }   
    
  }
      this.excelService.exportAsExcelFile(this.tabExcel, 'Liste congés');
 }
  search(value: string): void {
    this.item = this.items.filter((val) => val.agent.nom.toLowerCase().includes(value));
    this.collectionSize = this.item.length;
  }
  getDirections() {
    this.agentService.getAllDirections().subscribe((res) => {
      this.directions = res;
    });
  }

  filterByMonth() {
    this.congesService.getAllConges(this.dateInput);
  }

  statut(tracker: Tracker) {
    if(tracker.step === 1) {
      return 'label-light-warning'
    }else if(tracker.step === 2) {
      return 'label-light-success'
    }else if(tracker.step === 0) {
      return 'label-light-danger'
    }else if(tracker.step === 3) {
      return 'label-success'
    }
  }
  
  onClotureConges() {
    if (this.formCongesCloture.invalid) return;
  
    const selectedConges: number[] = this.formCongesCloture.value.checkCongesArray;

    selectedConges.forEach((congeId) => {
      this.congesService.getCongeById(congeId).subscribe(conge => {
        if(conge.tracker.step === 2) {
          this.trackerService.getTrackerStep(3).subscribe(tracker => {
            if(tracker) {
              conge.tracker = tracker;
              this.congeDataServ.getCongeData(conge.matricule).subscribe(cd => {
                if (cd) {
                  if (conge.nombreJourConge < 12) {
                    conge.nombreJourConge = cd.nbrJour
                  }else {
                    cd.nbrJourRecup += cd.nbrJour - conge.nombreJourConge;
                  }
                  cd.nbrJour = 0;
                  this.congesService.updateConge(conge).subscribe(
                    () => this.congeDataServ.updateCD(cd).subscribe(
                        () => {
                          this.modalService.dismissAll();
                          this.dateInput = 0;
                          this.congesService.getAllConges();
                        },
                        err => console.error(err)
                    ),
                    err => console.error(err)
                  )
                }
              }, err => console.error(err));
            }
          }, err => console.error(err))
        }
      }, err => console.error(err));
    });
  }

  onCheckboxChange(e) {
    const checkCongesArray: FormArray = this.formCongesCloture.get('checkCongesArray') as FormArray;

    if (e.target.checked) {
      // Add conge checked
      checkCongesArray.push(new FormControl(parseInt(e.target.value)));
    }else { // Unselected
      // Find the unselected number
      let i = 0;
      checkCongesArray.controls.forEach((ctrl: FormControl) => {
        if (ctrl.value === parseInt(e.target.value)) {
          checkCongesArray.removeAt(i);
          return;
        }
        i++;
      });
    }
    // console.log(checkCongesArray.value);
  }

  onSelectAll(event, conges: CongeModel[]) {
    const checkCongesArray: FormArray = this.formCongesCloture.get('checkCongesArray') as FormArray;
    while(checkCongesArray.length !== 0) {
      checkCongesArray.removeAt(0);
    }
    this.checkAll = event.target.checked;
    if (this.checkAll) {
      conges.forEach(conge => checkCongesArray.push(new FormControl(conge.id)));
    }
    // console.log(checkCongesArray.value);
  }
}
