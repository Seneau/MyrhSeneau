import { PlanificationComponent } from './planification/planification.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';

import { CongesRoutingModule } from './conges-routing.module';
import { CongesComponent } from './conges.component';
import { CongesListComponent } from './conges-list/conges-list.component';
import { CongesFormComponent } from './conges-form/conges-form.component';
import { RappelsFormComponent } from './rappels-form/rappels-form.component';
import { RappelsListComponent } from './rappels-list/rappels-list.component';
import { CongesValidationComponent } from './validations/conges-validation/conges-validation.component';
import { CongesPaieComponent } from './conges-paie/conges-paie.component';
import { RecuperationListComponent } from './recuperation-list/recuperation-list.component';
import { PoseRecupComponent } from './planification/pose-recup/pose-recup.component';
import { CoreModule } from 'src/app/_metronic/core';
import { ValidationsComponent } from './validations/validations.component';
import { RecuperationsValidationComponent } from './validations/recuperations-validation/recuperations-validation.component';
import { PoseCongesComponent } from './planification/pose-conges/pose-conges.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RappelsValidationComponent } from './validations/rappels-validation/rappels-validation.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';


@NgModule({
  declarations: [
    CongesComponent,
    CongesListComponent,
    CongesFormComponent,
    RappelsFormComponent,
    RappelsListComponent,
    CongesValidationComponent,
    CongesPaieComponent,
    RecuperationListComponent,
    PlanificationComponent,
    PoseRecupComponent,
    ValidationsComponent,
    RecuperationsValidationComponent,
    PoseCongesComponent,
    RappelsValidationComponent
  ],
  imports: [
    NgSelectModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    CommonModule,
    CongesRoutingModule,
    HttpClientTestingModule,
    CoreModule,
    FormsModule,
    NgbModule
  ]
})
export class CongesModule { }
