import { AuthwithkcService } from './../../auth/_services/authwithkc.service';
import { Component, OnInit } from '@angular/core';
import { AppStateEnum, AppDataState } from './../../../core/state/app-data.state';
import { CongeModel } from './../_models/conge.model';
import { Observable, of } from 'rxjs';
import { CongesService } from '../_services/conges.service';
import { concatMap, delay, startWith, catchError, map } from 'rxjs/operators';

@Component({
  selector: 'app-rappels-list',
  templateUrl: './rappels-list.component.html',
  styleUrls: ['./rappels-list.component.scss']
})
export class RappelsListComponent implements OnInit {
  months = [
    {value: 1, libelle: 'Janvier'}, 
    {value: 2, libelle: 'Février'}, 
    {value: 3, libelle: 'Mars'}, 
    {value: 4, libelle: 'Avril'}, 
    {value: 5, libelle: 'Mai'}, 
    {value: 6, libelle: 'Juin'}, 
    {value: 7, libelle: 'Juillet'}, 
    {value: 8, libelle: 'Aout'}, 
    {value: 9, libelle: 'Septembre'}, 
    {value: 10, libelle: 'Octobre'}, 
    {value: 11, libelle: 'Novembre'}, 
    {value: 12, libelle: 'Décembre'}
  ];
  searchInput: string;
  dateInput: number;
  
  readonly StateEnum = AppStateEnum; 
  rappels: Observable<AppDataState<CongeModel[]>>;
  page = 1
  pageSize =6;
  item : CongeModel[];
  items : CongeModel[];
  collectionSize:number=0;
  conges$: Observable<AppDataState<CongeModel[]>>;

  constructor(private congesService: CongesService, private authServ: AuthwithkcService) { }

  ngOnInit(): void {
    if (this.authServ.currentUserValue) {
      this.congesService.getCongesByResponsable(this.authServ.currentUserValue.attributes.matricule);
      this.rappels = this.congesService.conges$.pipe(
        map(conges => conges.filter(c => c.tracker.step === 3)),
        concatMap(el => of({state: this.StateEnum.LOADED, data: el})),
        startWith(({state: this.StateEnum.LOADING})),
        catchError(err => of({state: this.StateEnum.ERROR, errorMessage: err.message}))
      );    
      this.rappels.subscribe(el => {
        if(el.data!== undefined){
          this.items=el.data
          this.item=this.items;      
        }
       });
    }
  }
  
  search(value: string): void {
  
    this.item = this.items.filter((val) => val.agent.nom.toLowerCase().includes(value));
    this.collectionSize = this.item.length;
  }

  searchDate(value:string) {
    if(value!=null) {
     this.item=this.items.filter(
       x =>(new Date (x.dateDebut)).getMonth()==this.dateInput-1)
       this.collectionSize = this.item.length;
   }
   else {
    this.item=this.items;
    this.collectionSize = this.item.length;
    
   }
   
 }  

 
}
