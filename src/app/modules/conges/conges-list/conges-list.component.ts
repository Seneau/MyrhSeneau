import { AuthwithkcService } from './../../auth/_services/authwithkc.service';
import { AppStateEnum, AppDataState } from './../../../core/state/app-data.state';
import { CongeModel } from './../_models/conge.model';
import { Observable, of ,Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { CongesService } from '../_services/conges.service';
import { concatMap, delay, startWith, catchError, map } from 'rxjs/operators';


@Component({
  selector: 'app-conges-list',
  templateUrl: './conges-list.component.html',
  styleUrls: ['./conges-list.component.scss']
})
export class CongesListComponent implements OnInit {
  isLoading: boolean;
  private subscriptions: Subscription[] = [];
  page = 1
  pageSize =6;
  months = [
    {value: 1, libelle: 'Janvier'}, 
    {value: 2, libelle: 'Février'}, 
    {value: 3, libelle: 'Mars'}, 
    {value: 4, libelle: 'Avril'}, 
    {value: 5, libelle: 'Mai'}, 
    {value: 6, libelle: 'Juin'}, 
    {value: 7, libelle: 'Juillet'}, 
    {value: 8, libelle: 'Aout'}, 
    {value: 9, libelle: 'Septembre'}, 
    {value: 10, libelle: 'Octobre'}, 
    {value: 11, libelle: 'Novembre'}, 
    {value: 12, libelle: 'Décembre'}
  ];
  searchInput: string;
  dateInput: number;
  readonly StateEnum = AppStateEnum;
  conges$: Observable<AppDataState<CongeModel[]>>;
  item : CongeModel[];
  items : CongeModel[];
  collectionSize:number=0;
  constructor(private congesService: CongesService, private authServ: AuthwithkcService) { }

    
  ngOnInit(): void {
    if (this.authServ.currentUserValue) {
      this.congesService.getCongesByResponsable(this.authServ.currentUserValue.attributes.matricule);
  
      this.conges$ = this.congesService.conges$.pipe(
        concatMap(el => of({state: this.StateEnum.LOADED, data: el as CongeModel[]})),
        startWith(({state: this.StateEnum.LOADING})),
        catchError(err => of({state: this.StateEnum.ERROR, errorMessage: err.message}))
      );
      this.conges$.subscribe(el => {
        if(el.data!== undefined){
          this.items=el.data
          this.item=this.items;      
        }
      });
    }
  }

  search(value: string): void {
    this.item = this.items.filter((val) => val.agent.nom.toLowerCase().includes(value));
    this.collectionSize = this.item.length;
  }

  // dateFilter() {
  //   this.congesService.getCongesByResponsable(this.authServ.currentUserValue.attributes.matricule, this.dateInput);
   
  // }

  searchDate(value:string) {
     if(value!=null) {
      this.item=this.items.filter(
        x =>(new Date (x.dateDebut)).getMonth()==this.dateInput-1)
        this.collectionSize = this.item.length;
    }
    else {
     this.item=this.items;
     this.collectionSize = this.item.length;
     
    }
    
  }  

}
