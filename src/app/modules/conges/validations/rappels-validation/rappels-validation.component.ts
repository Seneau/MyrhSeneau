import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { map, concatMap, delay, startWith, catchError } from 'rxjs/operators';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { TrackersService } from './../../_services/trackers.service';
import { AuthwithkcService } from './../../../auth/_services/authwithkc.service';
import { RappelService } from './../../_services/rappel.service';
import { UserModel } from './../../../auth/_models/user.model';
import { RappelModel } from './../../_models/rappel.model';
import { Observable, of } from 'rxjs';
import { AppStateEnum, AppDataState } from './../../../../core/state/app-data.state';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rappels-validation',
  templateUrl: './rappels-validation.component.html',
  styleUrls: ['./rappels-validation.component.scss']
})
export class RappelsValidationComponent implements OnInit {

  months = [
    {value: 1, libelle: 'Janvier'}, 
    {value: 2, libelle: 'Février'}, 
    {value: 3, libelle: 'Mars'}, 
    {value: 4, libelle: 'Avril'}, 
    {value: 5, libelle: 'Mai'}, 
    {value: 6, libelle: 'Juin'}, 
    {value: 7, libelle: 'Juillet'}, 
    {value: 8, libelle: 'Aout'}, 
    {value: 9, libelle: 'Septembre'}, 
    {value: 10, libelle: 'Octobre'}, 
    {value: 11, libelle: 'Novembre'}, 
    {value: 12, libelle: 'Décembre'}
  ];
  page = 1
  pageSize =6;
  item : RappelModel[];
  items : RappelModel[];
  collectionSize:number=0;
  searchInput: string;
  dateInput: string;
  formRappelsValidation: FormGroup;
  checkAll = false;
  readonly StateEnum = AppStateEnum;
  rappels$: Observable<AppDataState<RappelModel[]>>;
  currentUser: UserModel;

  constructor(
    private fb: FormBuilder,
    private rappelService: RappelService,
    private authServ: AuthwithkcService,
    private trackerServ: TrackersService,
    config: NgbModalConfig, private modalService: NgbModal
  ) { 
    config.backdrop = 'static';
    config.keyboard = false;

    this.formRappelsValidation = this.fb.group({
      checkRappelsArray: this.fb.array([], Validators.required)
    })
  }

  openConfirmation(content) {
    this.modalService.open(content);
  }

  ngOnInit(): void {
    this.currentUser = this.authServ.currentUserValue;
    if (this.currentUser) {
      this.rappelService.getRappelsByDirecteur(this.authServ.currentUserValue.attributes.matricule);
      this.rappels$ = this.rappelService.rappels$.pipe(
        map(el => el.filter(x => x.tracker.step === 1)),
        concatMap(el => of({state: this.StateEnum.LOADED, data: el})),
        startWith(({state: this.StateEnum.LOADING})),
        catchError(err => of({state: this.StateEnum.ERROR, errorMessage: err.message}))
      );
  
      this.rappels$.subscribe(el => {
        if(el.data!== undefined){
          this.items=el.data
          this.item=this.items;      
        }
       });
    }
  }

  search(value: string): void {
    this.item = this.items.filter((val) => val.agent.nom.toLowerCase().includes(value));
    this.collectionSize = this.item.length;
  }
  onValidateOrRejectRappels(type: 'valid' | 'reject') {
    if (this.formRappelsValidation.invalid) return;

    const step = type === 'valid' ? 2 : type === 'reject' ? 0 : -1;
    const selectedRappels: number[] = this.formRappelsValidation.value.checkCongesArray;

    selectedRappels.forEach(rappelId => {
      this.trackerServ.getTrackerStep(step).subscribe(tracker => {
        this.rappelService.getRappel(rappelId).subscribe(rappel => {
          if (rappel.tracker.step === 1) {
            rappel.tracker = tracker;
            this.rappelService.updateRappel(rappel).subscribe(el => {
              if (el ) {
                // console.log(el);
                this.modalService.dismissAll();
                // this.rappelService.getRappelsByDirection(this.currentUser.attributes.matricule);
                this.rappelService.getRappelsByDirecteur(this.authServ.currentUserValue.attributes.matricule);
              } 
            });
          }
        }, err => console.error(err));
      }, err => console.error(err));
    });
  }

  onCheckboxChange(e) {
    const checkRappelsArray: FormArray = this.formRappelsValidation.get('checkRappelsArray') as FormArray;

    if (e.target.checked) {
      // Add conge checked
      checkRappelsArray.push(new FormControl(parseInt(e.target.value)));
    }else { // Unselected
      // Find the unselected number
      let i = 0;
      checkRappelsArray.controls.forEach((ctrl: FormControl) => {
        if (ctrl.value === parseInt(e.target.value)) {
          checkRappelsArray.removeAt(i);
          return;
        }
        i++;
      });
    }
    // console.log(checkRappelsArray);
  }

  onSelectAll(event, rappels: RappelModel[]) {
    const checkRappelsArray: FormArray = this.formRappelsValidation.get('checkRappelsArray') as FormArray;
    while(checkRappelsArray.length !== 0) {
      checkRappelsArray.removeAt(0);
    }
    this.checkAll = event.target.checked;
    if (this.checkAll) {
      rappels.forEach(conge => checkRappelsArray.push(new FormControl(conge.id)));
    }
    // console.log(checkRappelsArray.value);
  }
}
