import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { TrackersService } from '../../_services/trackers.service';
import { AuthwithkcService } from '../../../auth/_services/authwithkc.service';
import { concatMap, startWith, catchError, delay, filter, map } from 'rxjs/operators';
import { CongesService } from '../../_services/conges.service';
import { UserModel } from '../../../auth/_models/user.model';
import { CongeModel } from '../../_models/conge.model';
import { Observable, of } from 'rxjs';
import { AppStateEnum, AppDataState } from '../../../../core/state/app-data.state';
import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-conges-validation',
  templateUrl: './conges-validation.component.html',
  styleUrls: ['./conges-validation.component.scss']
})
export class CongesValidationComponent implements OnInit {

  months = [
    {value: 1, libelle: 'Janvier'}, 
    {value: 2, libelle: 'Février'}, 
    {value: 3, libelle: 'Mars'}, 
    {value: 4, libelle: 'Avril'}, 
    {value: 5, libelle: 'Mai'}, 
    {value: 6, libelle: 'Juin'}, 
    {value: 7, libelle: 'Juillet'}, 
    {value: 8, libelle: 'Aout'}, 
    {value: 9, libelle: 'Septembre'}, 
    {value: 10, libelle: 'Octobre'}, 
    {value: 11, libelle: 'Novembre'}, 
    {value: 12, libelle: 'Décembre'}
  ];

  formCongesValidation: FormGroup;
  page = 1
  pageSize =5;
  item : CongeModel[];
  items : CongeModel[];
  collectionSize:number=0;
  searchInput: string;
  checkAll = false;
  dateInput: number;
  readonly StateEnum = AppStateEnum;
  conges$: Observable<AppDataState<CongeModel[]>>;
  currentUser: UserModel;

  constructor(
    private fb: FormBuilder,
    private congesService: CongesService,
    private authServ: AuthwithkcService,
    private trackerServ: TrackersService,
    config: NgbModalConfig, private modalService: NgbModal
  ) { 
    config.backdrop = 'static';
    config.keyboard = false;
    this.formCongesValidation = this.fb.group({
      checkCongesArray: this.fb.array([], Validators.required)
    });
  }

  openConfirmation(content) {
    this.modalService.open(content);
  }

  ngOnInit(): void {
    this.currentUser = this.authServ.currentUserValue;
    // this.congesService.getCongesByDirection(this.currentUser.attributes.matricule);
    if (this.currentUser) {
      this.congesService.getCongesByDirection(this.authServ.currentUserValue.attributes.matricule);
      this.conges$ = this.congesService.conges$.pipe(
        map(el => el.filter(x => x.tracker.step === 1)),
        concatMap(el => of({state: this.StateEnum.LOADED, data: el})),
        startWith(({state: this.StateEnum.LOADING})),
        catchError(err => of({state: this.StateEnum.ERROR, errorMessage: err.message}))
      );
      // this.conges$.subscribe(el => console.log(el.data));
      this.conges$.subscribe(el => {
        if(el.data!== undefined){
          this.items=el.data
          this.item=this.items;      
        }
       });
    }
  }
  search(value: string): void {
    this.item = this.items.filter((val) => val.agent.nom.toLowerCase().includes(value));
    this.collectionSize = this.item.length;
  }

  searchDate(value:string) {
    if(value!=null) {
     this.item=this.items.filter(
       x =>(new Date (x.dateDebut)).getMonth()==this.dateInput-1)
       this.collectionSize = this.item.length;
   }
   else {
    this.item=this.items;
    this.collectionSize = this.item.length;
    
   }
   
 }  
 
  onValidateOrRejectConges(type: 'valid' | 'reject') {
    if (this.formCongesValidation.invalid) return;

    const step = type === 'valid' ? 2 : type === 'reject' ? 0 : -1;
    const selectedConges: number[] = this.formCongesValidation.value.checkCongesArray;
    
    selectedConges.forEach(congeId => {
      this.trackerServ.getTrackerStep(step).subscribe(tracker => {
        this.congesService.getCongeById(congeId).subscribe(conge => {
          if (conge.tracker.step === 1) {
            conge.tracker = tracker;
            this.congesService.updateConge(conge).subscribe(el => {
              if (el ) {
                // console.log(el);
                this.modalService.dismissAll();
                // this.congesService.getCongesByDirection(this.currentUser.attributes.matricule);
                this.congesService.getCongesByDirection(this.authServ.currentUserValue.attributes.matricule);
              } 
            });
          }
        })
      })
    });
  }

  onCheckboxChange(e) {
    const checkCongesArray: FormArray = this.formCongesValidation.get('checkCongesArray') as FormArray;

    if (e.target.checked) {
      // Add conge checked
      checkCongesArray.push(new FormControl(parseInt(e.target.value)));
    }else { // Unselected
      // Find the unselected number
      let i = 0;
      checkCongesArray.controls.forEach((ctrl: FormControl) => {
        if (ctrl.value === parseInt(e.target.value)) {
          checkCongesArray.removeAt(i);
          return;
        }
        i++;
      });
    }
    // console.log(checkCongesArray);
  }

  onSelectAll(event, conges: CongeModel[]) {
    const checkCongesArray: FormArray = this.formCongesValidation.get('checkCongesArray') as FormArray;
    while(checkCongesArray.length !== 0) {
      checkCongesArray.removeAt(0);
    }
    this.checkAll = event.target.checked;
    if (this.checkAll) {
      conges.forEach(conge => checkCongesArray.push(new FormControl(conge.id)));
    }
    // console.log(checkCongesArray.value);
  }

}
