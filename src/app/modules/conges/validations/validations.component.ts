import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-validations',
  templateUrl: './validations.component.html',
  styleUrls: ['./validations.component.scss']
})
export class ValidationsComponent implements OnInit {
  title: 'Validation congés';
  currentTab = 'conges'

  constructor() { }

  ngOnInit(): void {
  }

  setCurrentTab(tab: string) {
    this.currentTab = tab;
  }

}
