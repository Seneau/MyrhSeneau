import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';
import { CongeDataService } from './../../_services/conge-data.service';
import { map, concatMap, startWith, catchError, delay } from 'rxjs/operators';
import { Tracker } from './../../_models/tracker.model';
import { TrackersService } from './../../_services/trackers.service';
import { RecuperationModel } from './../../_models/recuperation.model';
import { Observable, of } from 'rxjs';
import { AppDataState } from './../../../../core/state/app-data.state';
import { RecuperationsService } from './../../_services/recuperations.service';
import { AuthwithkcService } from '../../../auth/_services/authwithkc.service';
import { UserModel } from '../../../auth/_models/user.model';
import { AppStateEnum } from '../../../../core/state/app-data.state';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recuperations-validation',
  templateUrl: './recuperations-validation.component.html',
  styleUrls: ['./recuperations-validation.component.scss']
})
export class RecuperationsValidationComponent implements OnInit {
  readonly StateEnum = AppStateEnum;
  formRecupsValidation: FormGroup;
  checkAll = false;
  currentUser: UserModel;
  recups$: Observable<AppDataState<RecuperationModel[]>>
  months = [
    {value: 1, libelle: 'Janvier'}, 
    {value: 2, libelle: 'Février'}, 
    {value: 3, libelle: 'Mars'}, 
    {value: 4, libelle: 'Avril'}, 
    {value: 5, libelle: 'Mai'}, 
    {value: 6, libelle: 'Juin'}, 
    {value: 7, libelle: 'Juillet'}, 
    {value: 8, libelle: 'Aout'}, 
    {value: 9, libelle: 'Septembre'}, 
    {value: 10, libelle: 'Octobre'}, 
    {value: 11, libelle: 'Novembre'}, 
    {value: 12, libelle: 'Décembre'}
  ];
  page = 1
  pageSize =6;
  item : RecuperationModel[];
  items : RecuperationModel[];
  collectionSize:number=0;
  searchInput: string;
  dateInput: number;
  constructor(
    private fb: FormBuilder,
    private recuperationServ: RecuperationsService,
    private authServ: AuthwithkcService,
    private trackerServ: TrackersService,
    private congeDataServ: CongeDataService,
    config: NgbModalConfig, private modalService: NgbModal
  ) { 
    config.backdrop = 'static';
    config.keyboard = false;

    this.formRecupsValidation = this.fb.group({
      checkRecupsArray: this.fb.array([], Validators.required)
    });
  }

  openConfirmation(content) {
    this.modalService.open(content);
  }

  ngOnInit(): void {
    this.currentUser = this.authServ.currentUserValue;

    if (this.currentUser) {      
      this.recuperationServ.getRecupsByParam({
        type: 'direction',
        matricule: this.currentUser.attributes.matricule[0]
      }, 1);
  
      this.recups$ = this.recuperationServ.recup$.pipe(
        concatMap(el => of({state: this.StateEnum.LOADED, data: el})),
        startWith(({state: this.StateEnum.LOADING})),
        catchError(err => of({state: this.StateEnum.ERROR, errorMessage: err.message}))
      );
      // this.conges$.subscribe(el => console.log(el.data));
      this.recups$.subscribe(el => {
        if(el.data!== undefined){
          this.items=el.data
          this.item=this.items;      
        }
       });
    }
  }

  search(value: string): void {
    this.item = this.items.filter((val) => val.agent.nom.toLowerCase().includes(value));
    this.collectionSize = this.item.length;
  }

  searchDate(value:string) {
    if(value!=null) {
     this.item=this.items.filter(
       x =>(new Date (x.dateRecup)).getMonth()==this.dateInput-1)
       this.collectionSize = this.item.length;
   }
   else {
    this.item=this.items;
    this.collectionSize = this.item.length;
    
   }
   
 }  
  onValidateOrRejectRecups(type: 'valid' | 'reject') {
    if (this.formRecupsValidation.invalid) return;

    const step = type === 'valid' ? 2 : type === 'reject' ? 0 : -1;
    const selectedRecups: number[] = this.formRecupsValidation.value.checkRecupsArray;
    
    selectedRecups.forEach(recupId => {
      this.trackerServ.getTrackerStep(step).subscribe(tracker => {
        this.recuperationServ.getRecuperation(recupId).subscribe(recup => {
          if (recup.tracker.step === 1) {
            recup.tracker = tracker;
            if (type === 'reject') {
              this.congeDataServ.getCongeData(recup.matricule).subscribe(
                cd => {
                  cd.nbrJourRecup += recup.nbrJour;
                  this.congeDataServ.updateCD(cd).subscribe(
                    () => console.log('ok')
                  )
                }
              )
            }
            this.recuperationServ.updateRecuperation(recup).subscribe(el => {
              if (el ) {
                // console.log(el);
                this.modalService.dismissAll();
                this.recuperationServ.getRecupsByParam({
                  type: 'direction',
                  matricule: this.currentUser.attributes.matricule
                }, 1);
              } 
            });
          }
        });
      }, err => console.error(err));
    });
    
  }
  
  onCheckboxChange(e) {
    const checkRecupsArray: FormArray = this.formRecupsValidation.get('checkRecupsArray') as FormArray;

    if (e.target.checked) {
      // Add conge checked
      checkRecupsArray.push(new FormControl(parseInt(e.target.value)));
    }else { // Unselected
      // Find the unselected number
      let i = 0;
      checkRecupsArray.controls.forEach((ctrl: FormControl) => {
        if (ctrl.value === parseInt(e.target.value)) {
          checkRecupsArray.removeAt(i);
          return;
        }
        i++;
      });
    }
    // console.log(checkRecupsArray);
  }

  onSelectAll(event, conges: RecuperationModel[]) {
    const checkRecupsArray: FormArray = this.formRecupsValidation.get('checkRecupsArray') as FormArray;
    while(checkRecupsArray.length !== 0) {
      checkRecupsArray.removeAt(0);
    }
    this.checkAll = event.target.checked;
    if (this.checkAll) {
      conges.forEach(conge => checkRecupsArray.push(new FormControl(conge.id)));
    }
    // console.log(checkRecupsArray.value);
  }

  statut(tracker: Tracker) {
    if(tracker.step === 1) {
      return 'label-light-warning'
    }else if(tracker.step === 2) {
      return 'label-light-success'
    }else if(tracker.step === 0) {
      return 'label-light-danger'
    }else if(tracker.step === 3) {
      return 'label-success'
    }
  }

}
