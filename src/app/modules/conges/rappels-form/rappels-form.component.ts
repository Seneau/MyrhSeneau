import { AgentsService } from './../../../core/services/agents.service';
import { TrackersService } from './../_services/trackers.service';
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { Observable } from "rxjs";
import Swal from "sweetalert2";
import { Agent } from "../../auth/_models/agent.model";
import { AuthwithkcService } from "../../auth/_services/authwithkc.service";
import { CongeModel } from "../_models/conge.model";
import { RappelModel } from "../_models/rappel.model";
import { CongesService } from "../_services/conges.service";

@Component({
  selector: "app-rappels-form",
  templateUrl: "./rappels-form.component.html",
  styleUrls: ["./rappels-form.component.scss"],
})
export class RappelsFormComponent implements OnInit {
  congeUpdate: CongeModel;
  formRappel: FormGroup;
  agents: Observable<Agent[]>;
  matricule: number;
  nomComplet: string;
  direction: string;
  etablissement: string;
  agent: any;
  submitted = false;
  rappeleApres = 0;
  rappelePour: number;
  nbrJrConge: number;
  rappelTab: [];
  maxRappelPour: number;

  constructor(
    private fb: FormBuilder,
    private authServ: AuthwithkcService,
    private congesService: CongesService,
    private router: Router,
    private route: ActivatedRoute,
    private agentService: AgentsService,
    private trackerService: TrackersService
  ) {
    this.formRappel = this.fb.group({
      rappeleApres: ["", [Validators.required]],
      rappelePour: ["", [Validators.required, Validators.min(1)]],
      motif: ["", Validators.required],
      dateRetour: ["", Validators.required],
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      if (params.id) {
        this.congesService.getCongeById(params.id).subscribe((conge) => {
          this.congeUpdate = conge;
          this.agentService
            .getAgentByMatricule(conge.matricule)
            .subscribe((agent) => {
              this.nomComplet = agent.nom;
              this.direction = agent.direction;
              this.etablissement = agent.etablissement;
              this.matricule = conge.matricule;
              this.nbrJrConge = conge.nombreJourConge;
              ///console.log(this.maxRappelPour);

              this.formRappel.controls["rappeleApres"].setValidators([
                Validators.required,
                Validators.min(12),
                Validators.max(this.nbrJrConge),
              ]);
              this.formRappel.controls["rappeleApres"].updateValueAndValidity();
            });
        });
      }
    });
  }

  get f(): any {
    return this.formRappel.controls;
  }

  datesFilter = (d: Date): boolean => {
    if (d) {
      const day = d.getDay();
      const now = new Date();

      const weekend = day !== 0 && day !== 6;

      if (d.getMonth() === 12) {
        return weekend && d.getFullYear() < now.getFullYear() + 1 && d > now;
      }

      return weekend && d.getFullYear() <= now.getFullYear() && d > now;
    }

    // return d > (new Date()) && weekend ;
  };

  //fonction onkeyup
  onKey(event: any) {
    this.rappeleApres = event.target.value;
    this.formRappel.controls["rappelePour"].setValidators([
      Validators.required,
      Validators.min(1),
      Validators.max(this.nbrJrConge - this.rappeleApres),
    ]);
    this.formRappel.controls["rappelePour"].updateValueAndValidity();
  }

  // fonction pour rappeler un employé en congé
  Rappeler() {
    Swal.fire({
      title: "Souhaitez vous  vraiment Rappeler cet employé ?",
      text: "Attention cet action est irreversible !",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Rappeler",
      cancelButtonText: "Annuler",
    }).then((result) => {
      if (result.value) {
        if (this.formRappel.invalid) return;
        if (!this.datesFilter(this.formRappel.value.dateRetour)) return;

        const rappel: RappelModel = this.formRappel.value;
        this.trackerService.getTrackerStep(1).subscribe(tracker => {
          if (tracker) {
            rappel.tracker = tracker;
            rappel.matricule = this.congeUpdate.matricule;
            this.congeUpdate.rappel = rappel;
            this.congesService.updateConge(this.congeUpdate).subscribe(
              () => {
                this.congesService.getCongesByResponsable(this.authServ.currentUserValue.attributes.matricule);
                this.router.navigateByUrl("/conges/rappels");
              }
            )
          }
        });

        // this.rappelService.Rappeler(rappel).subscribe((el) => {
        //   if (el) {
        //     this.router.navigateByUrl("/conges/rappels");
        //   }
        // });
        Swal.fire(
          "Éffectué !",
          "Le rappel est en attente d etre validé ",
          "success"
        );
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        this.router.navigateByUrl("/conges/rappels");
        Swal.fire("Annulé", " :)", "error");
      }
    });
  }
  // fin de la  fonction rappeler
}
