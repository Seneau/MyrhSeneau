import { RoleGuard } from './../auth/_services/role.guard';
import { PlanificationComponent } from './planification/planification.component';
import { ValidationsComponent } from './validations/validations.component';
import { RecuperationListComponent } from './recuperation-list/recuperation-list.component';
import { CongesFormComponent } from './conges-form/conges-form.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CongesListComponent } from './conges-list/conges-list.component';
import { RappelsListComponent } from './rappels-list/rappels-list.component';
import { RappelsFormComponent } from './rappels-form/rappels-form.component';
import { CongesPaieComponent } from './conges-paie/conges-paie.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'planifications',
    pathMatch: 'full'
  },
  {
    path: 'planifications',
    canActivate: [RoleGuard],
    data: {roles: ['ROLE_MANAGER', 'ROLE_DIRECTEUR', 'ROLE_MANAGER_PAIE', 'ROLE_DRHT']},
    component: CongesListComponent,
    children: [
      {
        path: ':id/reprogrammer',
        component: CongesFormComponent
      },
      {
        path: 'nouvelle',
        component: CongesFormComponent
      }
    ]
  },
  {
    path: 'validations',
    canActivate: [RoleGuard],
    data: {roles: ['ROLE_DIRECTEUR', 'ROLE_DRHT']},
    component: ValidationsComponent
  },
  {
    path: 'paye',
    canActivate: [RoleGuard],
    data: {roles: ['ROLE_DRHT', 'ROLE_AGENT_PAIE', 'ROLE_MANAGER_PAIE']},
    component: CongesPaieComponent
  },
  {
    path: 'demandes',
    component: PlanificationComponent
  },
  {
    path: 'rappels',
    canActivate: [RoleGuard],
    data: {roles: ['ROLE_MANAGER', 'ROLE_DIRECTEUR', 'ROLE_DRHT']},
    component: RappelsListComponent,
    children: [
      {
        path: '',
        redirectTo: 'rappels'
      },
      {
        path: ':id/rappeler',
        component: RappelsFormComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CongesRoutingModule { }
