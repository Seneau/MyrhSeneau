import { Tracker } from './tracker.model';
import { Agent } from './../../auth/_models/agent.model';
export class RecuperationModel {
    id?: number;
    matricule: number;
    nbrJour: number;
    motif: string;
    agent: Agent;
    tracker: Tracker;
    dateRecup: Date;
    canRecup?: boolean;
}