import { CongeModel } from 'src/app/modules/conges/_models/conge.model';
import { Agent } from './../../auth/_models/agent.model';
import { Tracker } from './tracker.model';
export class RappelModel {
    id?: number;
    dateRetour: Date;
    matricule: number;
    motif:string;
    rappeleApres:number;
    rappelePour:number;
    agent: Agent;
    tracker: Tracker;
    conge: CongeModel;
}
