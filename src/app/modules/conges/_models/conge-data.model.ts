import { Agent } from '../../auth/_models/agent.model';
import { CongeModel } from './conge.model';

export class CongeDataModel {
    id: number;
    matricule: number;
    nbrJour: number;
    nbrJourRecup: number;
    dateRetour: Date;
    agent:Agent;
}
