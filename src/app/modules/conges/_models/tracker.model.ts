export interface Tracker {
    id?: number;
    step?: number;
    libelle?: string;
}
