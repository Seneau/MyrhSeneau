import { RappelModel } from './rappel.model';
import { Tracker } from './tracker.model';
import { Agent } from './../../auth/_models/agent.model';
export interface CongeModel {
    id?: number;
    matricule: number;
    dateDebut: Date;
    dateRetourEffective: Date;
    agent: Agent;
    nombreJourConge: number;
    tracker: Tracker;
    canPose?: boolean;
    rappel: RappelModel;
}
