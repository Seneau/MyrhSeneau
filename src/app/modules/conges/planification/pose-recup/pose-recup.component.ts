import { TrackersService } from './../../_services/trackers.service';
import { UserModel } from './../../../auth/_models/user.model';
import { AuthwithkcService } from './../../../auth/_services/authwithkc.service';
import { concatMap, delay, startWith, catchError, map } from 'rxjs/operators';
import { CongeDataService } from './../../_services/conge-data.service';
import { RecuperationsService } from './../../_services/recuperations.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CongeDataModel } from './../../_models/conge-data.model';
import { RecuperationModel } from './../../_models/recuperation.model';
import { AppDataState, AppStateEnum } from './../../../../core/state/app-data.state';
import { Observable, of } from 'rxjs';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pose-recup',
  templateUrl: './pose-recup.component.html',
  styleUrls: ['./pose-recup.component.scss']
})
export class PoseRecupComponent implements OnInit {
  readonly StateEnum = AppStateEnum;
  recupState$: Observable<AppDataState<RecuperationModel[]>>;
  actuelRecup: RecuperationModel;
  congeData: CongeDataModel;
  formRecup: FormGroup;
  currentUser: UserModel;
  errorMessage = '';
  isEdit = false;
  page = 1
  pageSize =6;
  

  constructor(
    private fb: FormBuilder,
    private recupServ: RecuperationsService,
    private congeDataServ: CongeDataService,
    private authServ: AuthwithkcService,
    private trackerServ: TrackersService
  ) {
    this.formRecup = this.fb.group({
      dateRecup: [null, Validators.required],
      matricule: [null, Validators.required],
      motif: ['', [Validators.required, Validators.minLength(10)]],
      nbrJour: [null, [Validators.required, Validators.min(1)]]
    });
  }

  ngOnInit(): void {
    this.currentUser = this.authServ.currentUserValue;
    if (this.currentUser) {
      this.formRecup.get('matricule').patchValue(this.currentUser.attributes.matricule[0]);
      this.congeDataServ.getCongeDataByMatricule(this.currentUser.attributes.matricule);
  
  
      this.congeDataServ.congeData$.subscribe(
        cd => this.congeData = cd,
        err => console.error(err)
      );
      
      this.recupServ.getRecuperationsByMatricule(this.currentUser.attributes.matricule);
      
      this.recupState$ = this.recupServ.recup$.pipe(
        map(res => res.map(recup => {
          if (recup.tracker.step === 1){
            recup.canRecup = true;
          }else {
            recup.canRecup = false;
          }
          return recup;
        })),
        concatMap(el => of({state: this.StateEnum.LOADED, data: el})),
        startWith({state: this.StateEnum.LOADING}),
        catchError(err => of({state: this.StateEnum.ERROR, errorMessage: err.message}))
      );
    }
  }

  get f(): any { return this.formRecup.controls; }

  updateRecup(recup: RecuperationModel) {
    this.isEdit = true;
    this.actuelRecup = recup;
    this.formRecup.get('matricule').patchValue(this.actuelRecup.matricule);
    this.formRecup.get('dateRecup').patchValue(this.actuelRecup.dateRecup);
    this.formRecup.get('motif').patchValue(this.actuelRecup.motif);
    this.formRecup.get('nbrJour').patchValue(this.actuelRecup.nbrJour);
  }

  onSubmitRecup() {
    if (this.formRecup.invalid) return;

    if (this.isEdit) {
      const diffNbrJour = this.f.nbrJour.value - this.actuelRecup.nbrJour;
      // this.actuelRecup = this.formRecup.value;
      if (diffNbrJour > this.congeData.nbrJourRecup) {
        this.errorMessage = `Vous avez maximum ${this.congeData.nbrJourRecup} jours`;
        return;
      }
      // console.log(diffNbrJour, diffNbrJour > this.congeData.nbrJourRecup);
      const recupUpdated = Object.assign(this.actuelRecup, this.formRecup.value);

      this.trackerServ.getTrackerStep(1).subscribe(
        tracker => {
          recupUpdated.tracker = tracker;
          this.recupServ.updateRecuperation(recupUpdated).subscribe(
            () => {
              this.resetForm();
              this.congeData.nbrJourRecup = this.congeData.nbrJourRecup - diffNbrJour;
              this.congeDataServ.updateCongeData(this.congeData);
              this.recupServ.getRecuperationsByMatricule(this.currentUser.attributes.matricule);
            }
          )
        }
      );

    }else {
      if (this.f.nbrJour.value > this.congeData.nbrJourRecup) {
        this.errorMessage = `Vous avez maximum ${this.congeData.nbrJourRecup} jours`;
        return;
      }
      const recup: RecuperationModel = this.formRecup.value;
      this.trackerServ.getTrackerStep(1).subscribe(
        tracker => {
          recup.tracker = tracker;
          this.recupServ.createRecuperation(recup).subscribe(res => {
            if(res) {
              this.resetForm();
              this.congeData.nbrJourRecup -= recup.nbrJour;
              this.congeDataServ.updateCongeData(this.congeData);
              this.recupServ.getRecuperationsByMatricule(this.currentUser.attributes.matricule);
            }
          })
        }
      );
    }
  }

  nombreJourRecup(): number {
    return this.congeData && this.congeData.nbrJourRecup
      ? this.congeData.nbrJourRecup
      : 0;
  }

  resetForm() {
    this.formRecup.reset();
    this.errorMessage = '';
    this.isEdit = false;
  }

  datesFilter = (d: Date): boolean => {
    if (d) {
      
      const day = d.getDay();
      const now = new Date();
      
      const weekend = day !== 0 && day !== 6;

      if (d.getMonth() === 12) {
        return weekend && d.getFullYear() < now.getFullYear() + 1 && d > now;
      }

      return weekend && d.getFullYear() <= now.getFullYear() && d > now;
    }
    
    // return d > (new Date()) && weekend ;
  }

}
