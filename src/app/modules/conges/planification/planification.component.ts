import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-planification',
  templateUrl: './planification.component.html',
  styleUrls: ['./planification.component.scss']
})
export class PlanificationComponent implements OnInit {
  currentTab = 'recuperations';
  formPlanification: FormGroup;
  constructor( private fb: FormBuilder) { 

    this.formPlanification = this.fb.group({
      nbre: [null, Validators.required]
    });
  }

  ngOnInit(): void {
  }

  setCurrentTab(tab: string): void {
    this.currentTab = tab;
  }
  
}
