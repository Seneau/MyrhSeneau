import { CongeDataService } from './../../_services/conge-data.service';
import { CongeDataModel } from './../../_models/conge-data.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { concatMap, delay, startWith, catchError, map } from 'rxjs/operators';
import { AppStateEnum, AppDataState } from './../../../../core/state/app-data.state';
import { AuthwithkcService } from './../../../auth/_services/authwithkc.service';
import { UserModel } from './../../../auth/_models/user.model';
import { CongeModel } from 'src/app/modules/conges/_models/conge.model';
import { Observable, of } from 'rxjs';
import { CongesService } from './../../_services/conges.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pose-conges',
  templateUrl: './pose-conges.component.html',
  styleUrls: ['./pose-conges.component.scss']
})
export class PoseCongesComponent implements OnInit {
  readonly StateEnum = AppStateEnum;
  mesConges$: Observable<AppDataState<CongeModel[]>>
  congeData: CongeDataModel;
  congeSelected: CongeModel;
  actuelConge: CongeModel;
  currentUser: UserModel;
  formConge: FormGroup;

  page = 1
  pageSize =6;
  
  

  constructor(
    private fb: FormBuilder,
    private congesServ: CongesService,
    private authServ: AuthwithkcService,
    private congeDataServ: CongeDataService
  ) {
    this.formConge = this.fb.group({
      nombreJourConge: [null, [Validators.required, Validators.min(12)]],
      dateDebut: [null, Validators.required]
    });
  }

  ngOnInit(): void {
    this.currentUser = this.authServ.currentUserValue;
    // this.congesService.getCongesByDirection(this.currentUser.attributes.matricule);
    if (this.currentUser) {
      this.congesServ.getCongesByMatricule(this.authServ.currentUserValue.attributes.matricule);
  
      this.congeDataServ.getCongeDataByMatricule(this.currentUser.attributes.matricule);
  
      this.congeDataServ.congeData$.subscribe(
          cd => this.congeData = cd,
          err => console.error(err)
      );
  
      this.mesConges$ = this.congesServ.conges$.pipe(
        map(res => {
          const tab = res.filter(c => c.tracker.step === 1 || c.tracker.step === 2);
          tab.sort((a,b) => {
            return new Date(a.dateDebut).getTime() - new Date(b.dateDebut).getTime()}
          );
          // console.log('tab ', tab);
          res.map(conge => {
            if (conge.id === tab[0].id) {
              this.actuelConge = tab[0];
              conge.canPose = true;
            }else {
              conge.canPose = false;
            }
          });
          // console.log(res);
          return res;
        }),
        concatMap(el => of({state: this.StateEnum.LOADED, data: el})),
        startWith(({state: this.StateEnum.LOADING})),
        catchError(err => of({state: this.StateEnum.ERROR, errorMessage: err.message}))
      );
    }
  }

  
  choisirConge(conge: CongeModel) {
    this.congeSelected = conge;
    this.formConge.get('nombreJourConge').setValue(conge.nombreJourConge);
    this.formConge.get('dateDebut').setValue(conge.dateDebut);
  }

  disablePoseConge(conge: CongeModel): boolean {
    return conge.tracker.step === 0 || conge.tracker.step === 3 || !conge.canPose;
  }
  
  updateConge() {
    console.log(this.formConge.value.dateDebut, this.congeSelected.dateDebut !== this.formConge.value.dateDebut);
    if (this.formConge.invalid || this.congeSelected.dateDebut !== this.formConge.value.dateDebut) return;
    console.log('ok');
    this.congeSelected.nombreJourConge = this.formConge.value.nombreJourConge;

    this.congesServ.updateConge(this.congeSelected).subscribe(
      conge => {
        if (conge) {
          this.formConge.reset();
        }
      }, err => console.error(err)
    )
  }

  nombreJourCongeActuel(): number {
    if (!this.actuelConge && this.congeData) {
      return this.congeData.nbrJour;
    }
    return (this.congeData && this.actuelConge && this.congeData.nbrJour > this.actuelConge.nombreJourConge) 
      ? this.congeData.nbrJour - this.actuelConge.nombreJourConge
      : 0;
  }
}
