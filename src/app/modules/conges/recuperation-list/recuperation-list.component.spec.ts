import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecuperationListComponent } from './recuperation-list.component';

describe('RecuperationListComponent', () => {
  let component: RecuperationListComponent;
  let fixture: ComponentFixture<RecuperationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecuperationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecuperationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
