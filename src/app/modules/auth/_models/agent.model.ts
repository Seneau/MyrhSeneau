export class Agent {
    id?: number;
    direction: string;
    equipe: number;
    etablissement: string;
    fonction: string;
    matricule: string;
    matriculeN1: string;
    nom: string;
    nomN1: string;
    serviceSecteur: string;
    statut: string;
    taux: number;
}