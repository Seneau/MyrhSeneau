export class UserModel {
  id: number;
  username: string;
  email: string;
  firstname: string;
  lastname: string;
  attributes: {
    matricule: number
  }
}
