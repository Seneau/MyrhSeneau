import { environment } from './../../../../environments/environment';
import { KeycloakInstance } from 'keycloak-js';
import { Injectable } from '@angular/core';
declare var Keycloak: any;


@Injectable({providedIn: 'root'})
export class KeycloackSecurityService{

    public kc: KeycloakInstance;
    public async init()
    {
        this.kc = new Keycloak({
            url: `${environment.keycloak.issuer}`,
            realm: `${environment.keycloak.realm}`,
            clientId: `${environment.keycloak.clientId}`
        });
        await this.kc.init({
           onLoad: 'login-required'
        });
    }


}
