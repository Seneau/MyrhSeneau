import { Injectable } from "@angular/core";
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from "@angular/router";
import { KeycloackSecurityService } from "./keycloak-security.service";

@Injectable({ providedIn: "root" })
export class RoleGuard implements CanActivate {
  constructor(private router: Router, private ks: KeycloackSecurityService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // return ;
    if (!this.ks.kc.authenticated) {
      // logged in so return true
      this.ks.kc.login({
        redirectUri: window.location.origin + state.url,
      });
    }
    const requiredRoles = route.data.roles;

    if (!(requiredRoles instanceof Array) || requiredRoles.length === 0) {
      return true;
    }

    let returnBool = false;

    requiredRoles.forEach((roleReq) => {
      if (this.ks.kc.hasRealmRole(roleReq)) {
        returnBool = true;
      }
    });

    if (!returnBool) {
      this.router.navigate(['dashboard']);
    }
    return returnBool;
  }
}
