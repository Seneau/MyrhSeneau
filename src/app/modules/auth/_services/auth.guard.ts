import { KeycloakInstance } from 'keycloak-js';
import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import { KeycloackSecurityService } from './keycloak-security.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  
  constructor(private router: Router, private ks: KeycloackSecurityService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // return ;
    if (!this.ks.kc.authenticated) {
      // logged in so return true
      this.ks.kc.login({
         redirectUri: window.location.origin + state.url,
       });
    }
    const requiredRoles = route.data.roles;

    if (!(requiredRoles instanceof Array) || requiredRoles.length === 0) {
       return true;
    }
    let returnBool = false 
    requiredRoles.forEach( roleReq => { if (this.ks.kc.hasResourceRole(roleReq)){  returnBool=true ; }});

    return returnBool;
  //   if (requiredRoles.every((role) => this.ks.kc.hasResourceRole(role))) {
  //     return true;
  //   } else {
  //     // redirect to error page if the user doesn't have the nessecairy  role to access
  //     // we will define this routes in a bit

  //     return false;

  //   // not logged in so redirect to login page with the return url
  //   // this.authService.logout();
  //   // return false;

   }
}
