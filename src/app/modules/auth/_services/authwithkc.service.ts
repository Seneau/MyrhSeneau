import { KeycloackSecurityService } from './keycloak-security.service';
import { UserModel } from './../_models/user.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthwithkcService {
  private currentUserSubject = new BehaviorSubject<any>(undefined);

  constructor(private ks: KeycloackSecurityService) { }
  
  loadKcService(){
    this.ks.kc.loadUserProfile()
      .then(data => {
        this.currentUserSubject.next(data);
      });
  }

  getCurrentUser(): Observable<UserModel> {
    return this.currentUserSubject.asObservable();
  }

  get currentUserValue(): UserModel {
    return this.currentUserSubject.value;
  }

  logout() {
    this.ks.kc.logout();
  }

  isDirecteur(): boolean {
    return this.ks.kc.hasRealmRole("ROLE_DIRECTEUR");
  }

  isManager(): boolean {
    return this.ks.kc.hasRealmRole("ROLE_MANAGER");
  }

  getCurrentRoleUser() {
    let roleUser = null;
    const allRoleUser = this.ks.kc.realmAccess.roles;

    allRoleUser.forEach((role) => {
      if (role.includes("ROLE_")) {
        roleUser = role.substr(5);
      }
    });
    return roleUser;
  }

  getCurrentMatriculeUser() {
    return this.ks.kc.profile['attributes'];
  }
}
