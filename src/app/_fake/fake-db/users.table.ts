export class UsersTable {
  public static users: any = [
    {
      id: 1,
      username: 'admin',
      password: 'demo',
      email: 'manager@demo.com',
      accessToken: 'access-token-8f3ae836da744329a6f93bf20594b5cc',
      refreshToken: 'access-token-f8c137a2c98743f48b643e71161d90aa',
      roles: [1], // Manager
      pic: './assets/media/users/300_25.jpg',
      matricule: 14126,
      fullname: 'PAPE DEMBA WAR FALL'
    },
    {
      id: 2,
      username: 'directeur',
      password: 'demo',
      email: 'directeur@demo.com',
      accessToken: 'access-token-6829bba69dd3421d8762-991e9e806dbf',
      refreshToken: 'access-token-f8e4c61a318e4d618b6c199ef96b9e55',
      roles: [2], // Directeur
      pic: './assets/media/users/100_2.jpg',
      matricule: 13539,
      fullname: 'YAMA DIEYE'
    },
    {
      id: 3,
      username: 'rh',
      password: 'demo',
      email: 'rh@demo.com',
      accessToken: 'access-token-d2dff7b82f784de584b60964abbe45b9',
      refreshToken: 'access-token-c999ccfe74aa40d0aa1a64c5e620c1a5',
      roles: [3], // RH
      pic: './assets/media/users/default.jpg',
      matricule: 15146,
      fullname: 'SIRA DIOP'
    },
    {
      id: 4,
      username: 'rhManager',
      password: 'demo',
      email: 'rhmanager@demo.com',
      accessToken: 'access-token-d2dff7b82f784de584b60964abbe45b4',
      refreshToken: 'access-token-c999ccfe74aa40d0aa1a64c5e620c1a6',
      roles: [4], // RHManager
      pic: './assets/media/users/default.jpg',
      matricule: 13591,
      fullname: 'Made FALL'
    },
    {
      id: 5,
      username: 'collab',
      password: 'demo',
      email: 'collab@demo.com',
      accessToken: 'access-token-d2dff7b82f784de584b60964abbe45a8',
      refreshToken: 'access-token-c999ccfe74aa40d0aa1a64c5e620c1b5',
      roles: [5], // Collaborateur
      pic: './assets/media/users/default.jpg',
      matricule: 13591,
      fullname: 'Made FALL'
    },
  ];

  public static tokens: any = [
    {
      id: 1,
      accessToken: 'access-token-' + Math.random(),
      refreshToken: 'access-token-' + Math.random(),
    },
  ];
}
